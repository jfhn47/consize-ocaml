# Notes on type checking in Consize

## Type inference

The inference algorithm takes a quotation and tries to infer its type.
For example, the quotation `[ 1 2 dup ]` gets inferred to `[ int int int ]`.
The algorithm runs the program on the abstract type level by inferring literals (i.e. integers or words) or taking the type signature of already defined words.

We keep track of the types with a special type stack, that contains the types of items on the data stack.
Consider a larger example for `[ 1 ] 1 dup rot swap`.
The format for the example is `types = [ ... ] | <next item> | <next item type>`.

```
types = ε                   | [ 1 ]                                 | [ int ]
types = [ int ]             | 1                                     | int
types = [ int ] int         | dup  [ ∀ a . a -- a a ]               | a = int
types = [ int ] int int     | rot  [ ∀ a b c . a b c -- b c a ]     | a = [ int ], b = int, c = int
types = int int [ int ]     | swap [ ∀ a b . a b -- b a ]           | a = int, b = [ int ]
types = int [ int ] int     | done
```

### Instantiate

Example:

```
pt = forall a . a -- a a
instantiate(pt, int) ==> [a -> int]
a -- a a [a -> int] => int -- int int
```

### Unification

Example with `push`:

```
[ int ] wrd | push [ ∀ a b . [ a ] b -- [ a b ] ]
===>
    unify [ a ] b with [ int ] wrd
    ==> unify [ a ] with [ int ]
        ==> unify a with int
        <== a = int
    <== a = int
    ==> unify b with wrd
    <== b = wrd
    instantiate with a = int, b = wrd
    [ int ] wrd -- [ int wrd ]
<=== [ int wrd ]
[ int wrd ]
```

Examples with `apply [ ∀ a b . [ a ] [ a -- b ] -- [ b ] ]`:

```
[ int ] [ int -- wrd ] | apply [ ∀ a b . [ a ] [ a -- b ] -- [ b ] ]
===>
    unify [ int ] [ int -- wrd ] with [ a ] [ a -- b ]
    ==> unify [ int ] with [ a ]
        ==> unify int with a
        <== a := int
    <== a = int
    ==> unify [ int -- wrd ] with [ a -- b ]
        ==> unify int with a
        <== a := int
        ==> unify wrd with b
        <== b := wrd
    <== a = int, b = wrd
    instantiate with a = int, b = wrd
    [ int ] [ int -- wrd ] -- [ wrd ]
<=== [ wrd ]
[ wrd ]
```

```
[ int ] [ [ int ] int -- [ int ] ] | apply [ ∀ a b . [ a ] [ a -- b ] -- [ b ] ]
===>
    unify [ int ] [ [ int ] int -- [ int ] ] with [ ∀ a b . [ a ] [ a -- b ] -- [ b ] ]
    ==> unify [ int ] with [ a ]
        ==> unify int with a
        <== a = int
    <== a = int
    ==> unify [ [ int ] int -- [ int ] ] with [ a -- b ]
        ==> unify [ int ] int with a
        <== error: a := int != a := [ int ] int
    <== error
<===
error: expected int but got [ int ] int where a = int
```



## Inference of quotations

```
[ 1 [ ] a [ swap dup ] rot ]

start with [], []
==> 1 :: [ [ ] a [ swap dup ] rot ]
    infer 1 => Int
<== [ Int ], [ ]
==> [ ] :: [ a [ swap dup ] rot ]
    infer [ ] => [ ? ]
<== [ Int [ ? ] ], [ ]
==> a :: [ [ swap dup ] rot ]
    infer a => Wrd
<== [ Int [ ? ] Wrd ], [ ]
==> [ swap dup ] :: [ rot ]
    infer [ swap dup ]
    start with [], [ ]
    ==> swap :: [ dup ]
        infer swap => [ forall a b . a b -- b a ] => [ a? b? -- b? a? ]
        fill up in stack with [ ? ? ]
        unify [ ? ? -- ret? ] with [ a? b? -- b? a? ]
    <== [ ? ? ], [ ? ? ]
    ==> dup :: [ ]
        infer dup => [ forall a . a -- a a ] => [ a? -- a? a? ]
        unify [ ? ? -- ? ? ] with [ a? -- a? a? ]
    <== [ ? ? ], [ ? ? ? ]
<== [ Int [ ? ] Wrd [ ? ? -- ? ? ? ] ], []
==> rot :: [ ]
    infer rot => [ forall a b c . a b c -- c a b ] => [ a? b? c? -- c? a? b? ]
    unify [ [ ? ] Wrd [ ? ? -- ? ? ? ] -- ret? ] with [ a? b? c? -- c? a? b? ] => [ [ ? ] Wrd [ ? ? -- ? ? ? ] -- [ ? ? -- ? ? ? ] [ ? ] Wrd ]
    max argument depth = 3
<== [ Int [ ? ] Wrd [ ? ? -- ? ? ? ] ], [ [ ? ? -- ? ? ? ] [ ? ] Wrd ]
    write everything with depth > 3 to the out stack.
<== [ Int [ ? ] Wrd [ ? ? -- ? ? ? ] ], [ Int [ ? ? -- ? ? ? ] [ ? ] Wrd ]
```

```
[ 2drop ] ==> [ drop drop ]

start with [], []
==> drop :: [ drop ]
    infer drop => [ forall a . a -- ] => [ a? -- ]
    fill up input stack with 1 unknown
    unify [ ? -- ] with [ a? -- ]
<== [ ? -- ], [ ]
==> drop :: [ ]
    infer drop => [ forall a . a -- ] => [ a? -- ]
```
