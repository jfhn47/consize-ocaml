% general util functions

: print-string ( quot -- ) ( ) [ push <space> push ] reduce pop reverse word println ;

%%% test typing.read-type and typing.show-type

[ =========== Testing typing.read-type and typing.show-type =========== ] print-string

%% mono types with words and functions
( Int ) [ [ Int ] typing.read-type typing.show-type ] unit-test
( ( Int -- Int ) repr ) [ [ Int -- Int ] typing.read-type typing.show-type ] unit-test
( ( Int Int -- Int Int ) repr ) [ [ Int Int -- Int Int ] typing.read-type typing.show-type ] unit-test
( ? ) [ [ ? ] typing.read-type typing.show-type ] unit-test   % unknown type

%% mono types with stacks and maps
( ( [ Int ] repr ) word ) [ [ [ Int ] ] typing.read-type typing.show-type ] unit-test
( ( [ Int Wrd [ Int ] ] repr ) word ) [ [ [ Int Wrd [ Int ] ] ] typing.read-type typing.show-type ] unit-test
( ( { [ Wrd ] [ Int ] } repr ) word ) [ [ { [ Wrd ] [ Int ] } ] typing.read-type typing.show-type ] unit-test
( ( { [ Wrd [ Int ] Int ] [ Int [ Wrd ] Wrd ] } repr ) word ) [ [ { [ Wrd [ Int ] Int ] [ Int [ Wrd ] Wrd ] } ] typing.read-type typing.show-type ] unit-test

%% poly types with words and functions
% signature of dup
( ( forall A . A -- A A ) repr ) [ [ forall A . A -- A A ] typing.read-type typing.show-type ] unit-test
% signature of swap
( ( forall A B . A B -- B A ) repr ) [ [ forall A B . A B -- B A ] typing.read-type typing.show-type ] unit-test

%% poly types with stacks and maps
% signature of push
( ( forall A B . [ A ] repr B -- [ A B ] repr ) repr ) [ [ forall A B . [ A ] B -- [ A B ] ] typing.read-type typing.show-type ] unit-test
% signature of mapping
( ( forall A . [ A ] repr -- { [ A ] [ A ] } repr ) repr ) [ [ forall A . [ A ] -- { [ A ] [ A ] } ] typing.read-type typing.show-type ] unit-test
% signature of unmap
( ( forall A B . { [ A ] [ B ] } repr -- [ A B ] repr ) repr ) [ [ forall A B . { [ A ] [ B ] } -- [ A B ] ] typing.read-type typing.show-type ] unit-test



%%% test typing.infer

[ =========== Testing typing.infer =========== ] print-string

% simple tests
( ( Int ) ) [ [ 1 ] typing.infer typing.show-types ] unit-test
( ( Int ) ) [ [ 1 2 + ] typing.infer typing.show-types ] unit-test
( ( Int [ Int ] repr Int ) ) [ [ [ 1 ] 1 dup rot swap ] typing.infer typing.show-types ] unit-test

% tests with type matches on stacks
( ( [ Int Wrd ] repr ) ) [ [ [ 1 ] a push ] typing.infer typing.show-types ] unit-test
( ( { [ Int Wrd ] [ Wrd Int ] } repr ) ) [ [ 1 a { 1 a } assoc ] typing.infer typing.show-types ] unit-test

% tests on quotations
( ( ( ? ? -- ) ) repr ) [ [ [ 2drop ] ] typing.infer typing.show-types repr ] unit-test
( ( ( ? ? ? -- ? ) ) repr ) [ [ [ rot 2drop ] ] typing.infer typing.show-types repr ] unit-test
( ( Int ( ? ? -- ? ? ? ) repr Wrd Int ) repr ) [ [ 1 2 a [ swap dup ] rot ] typing.infer typing.show-types repr ] unit-test

%%% test typing.check

[ =========== Testing typing.check =========== ] print-string


% simple tests --- note that the quotations top is right (we push from left to right) and type stacks have a top on the left.
[ t ] [ [ 5 a ] [ Wrd Int ] typing.read-types typing.check ] unit-test

% test typed definitions --- they are not printed but no error implies that term and type match.
:: five [ Int ] 5 ;
:: type-list [ Wrd Int ] 1 a ;
:: inc [ [ Int -- Int ] ] 1 + ;
:: six [ Int ] five inc ;
% :: ill-typed [ Int ] a ;  % this is ill typed, hence not executed

:: my-dup [ [ ? -- ? ? ] ] dup ;
:: my-poly-dup [ [ forall a . a -- a a ] ] dup ;
% :: ill-typed-poly-dup [ [ forall a . a -- a a ] ] swap ;


%%% printing of all builtin function types

% \ swap typing.
% \ swap typing.source2
% [ [ swap ] ] typing.infer typing.show-types repr
% \ dup typing.source
% \ drop typing.
% \ rot typing.sourc
% \ type typing.source
% \ equal? typing.source
% \ identical? typing.source
% \ emptystack typing.source
% \ push typing.source
% \ top typing.source
% \ pop typing.source
% \ concat typing.source
% \ reverse typing.source
% \ mapping typing.source
% \ unmap typing.source
% \ assoc typing.source
% \ dissoc typing.source
% \ get typing.source
% \ keys typing.source
% \ merge typing.source
% \ word typing.source
% \ unword typing.source
% \ char typing.source
% \ print typing.source
% \ flush typing.source
% \ read-line typing.source
% \ slurp typing.source
% \ spit typing.source
% \ spit-on typing.source
% \ uncomment typing.source
% \ tokenize typing.source
% \ undocument typing.source
% \ current-time-millis typing.source
% \ operating-system typing.source
% \ call typing.source
% \ call/cc typing.source
% \ continue typing.source
% \ get-dict typing.source
% \ set-dict typing.source
% \ apply typing.source
% \ compose typing.source
% \ func typing.source
% \ integer? typing.source
% \ + typing.source
% \ - typing.source
% \ * typing.source
% \ div typing.source
% \ mod typing.source
% \ < typing.source
% \ > typing.source
% \ == typing.source
% \ <= typing.source
% \ >= typing.source
% \ stepcc typing.source
% \ load typing.source
% \ run typing.source
