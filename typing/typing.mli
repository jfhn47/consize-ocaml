open Common
open Item

val check : stack -> stack option

(** typing.infer ( quot -- type ) *)
val infer : stack -> stack option

(** typing.read-type ( wrd -- type ) *)
val read_type : stack -> stack option

(** typing.show-type ( type -- wrd ) *)
val show_type : stack -> stack option

(** typing.type-info ( type -- wrd ) *)
val type_info : stack -> stack option

(* This may eventually get replaced by a library function. *)

(** typing.source ( type -- wrd ) *)
val source : stack -> stack option
