open Common
open Cs_type
open Util
open Item

let show_unify_logs : bool = false

let show_inference_logs : bool = false

let ( let* ) = Option.bind

(** Replaces all bound variables from a quantified type with fresh meta variables. *)
let instantiate ?(new_type_vars : bool = false) : Cs_type.t -> Cs_type.mono =
  let open Cs_type in
  (* Type vars are reversed => levels and not indices for easier access. *)
  let rec aux (tvs : string list) (mvars : meta_var option array) : mono -> mono
    = function
    | Bound index ->
      (match mvars.(index) with
       | Some mvar -> Meta mvar
       | None ->
         let mvar = fresh_meta_var (List.nth tvs index) in
         mvars.(index) <- Some mvar;
         Meta mvar)
    | Cs_type.Fun (is, os) ->
      (is, os)
      |> Tuple.map (List.map (aux tvs mvars))
      |> fun (is, os) -> Cs_type.Fun (is, os)
    | Cs_type.Stack stack -> stack |> List.map (aux tvs mvars) |> fun s -> Cs_type.Stack s
    | Cs_type.Map (ks, vs) ->
      (ks, vs)
      |> Tuple.map (List.map (aux tvs mvars))
      |> fun (ks, vs) -> Cs_type.Map (ks, vs)
    | mono -> mono
  in
  function
  | Mono mono -> mono
  | Poly (tvs, mono) ->
    let tv_count = List.length tvs in
    let mvars : meta_var option array = Array.make (List.length tvs) None in
    let tvs' =
      if new_type_vars
      then
        Seq.take tv_count Seq2.nats0
        |> Seq.map (( ^ ) "t" @$ string_of_int)
        |> List.of_seq
      else tvs
    in
    aux tvs' mvars mono
;;

exception Unification_error of string

(** Checks whether a meta variable occurs in another, leading to an infinite type. *)
let rec occurrence_check (mv : Cs_type.meta_var) : Cs_type.mono -> bool = function
  | Meta { instance = Link mv'; _ } -> occurrence_check mv (Cs_type.Meta mv')
  | Meta { instance = Just t'; _ } -> occurrence_check mv t'
  | Meta { instance = Undefined; id; _ } -> mv.id = id
  | Stack ts -> List.exists (occurrence_check mv) ts
  | Map (xs, ys) | Fun (xs, ys) ->
    List.exists (occurrence_check mv) xs || List.exists (occurrence_check mv) ys
  | _ -> false
;;

(** Tries to unify two types, throws Unification_error on failure. Use in try-with. *)
let unify (t1 : Cs_type.mono) (t2 : Cs_type.mono) : unit =
  let open Cs_type in
  let open Printf in
  if show_unify_logs
  then printf "unify '%s' with '%s'\n" (show (Mono t1)) (show (Mono t2));
  let indent_str (indent : int) : string =
    Seq.repeat ' ' |> Seq.take (indent * 4) |> List.of_seq |> String2.implode
  in
  let rec unify_one (indent : int) (t1 : mono) (t2 : mono) : unit =
    let pt1, pt2 = Mono t1, Mono t2 in
    if show_unify_logs
    then printf "%s==> unify '%s' with '%s'\n" (indent_str indent) (show pt1) (show pt2);
    match t1, t2 with
    | Free n1, Free n2 when n1 = n2 ->
      if show_unify_logs
      then
        printf "%s<== %s = %s   (free, free)\n" (indent_str indent) (show pt1) (show pt2)
    | Bound i1, Bound i2 when i1 = i2 ->
      () (* TODO: Can this case even happen after an instantiation? *)
    | Stack s1, Stack s2 when List.length s1 = List.length s2 ->
      if show_unify_logs
      then
        printf
          "%sunify %s with %s   (stack, stack)\n"
          (indent_str (indent + 1))
          (show pt1)
          (show pt2);
      unify_many (indent + 1) s1 s2 |> ignore
    | Fun (x1, x2), Fun (y1, y2)
      when List.length x1 = List.length y1 && List.length x2 = List.length y2 ->
      unify_many (indent + 1) x1 y1 |> ignore;
      unify_many (indent + 1) x2 y2 |> ignore
    | Map (x1, x2), Map (y1, y2)
      when List.length x1 = List.length y1 && List.length x2 = List.length y2 ->
      unify_many (indent + 1) x1 y1 |> ignore;
      unify_many (indent + 1) x2 y2 |> ignore
    | Meta mv1, Meta mv2 when mv1.id = mv2.id ->
      if show_unify_logs
      then printf "%s<== %s = %s   (mv = mv)\n" (indent_str indent) (show pt1) (show pt2)
    | Meta mv1, Meta mv2 ->
      let mv1' = prune_meta mv1 in
      let mv2' = prune_meta mv2 in
      if mv1'.id <> mv2'.id
      then (
        if show_unify_logs
        then
          printf "%s<== %s := %s   (mv, mv)\n" (indent_str indent) (show pt1) (show pt2);
        match mv1'.instance, mv2'.instance with
        | Undefined, _ -> mv1'.instance <- Link mv2'
        | _, Undefined -> mv2'.instance <- Link mv1'
        | _, _ -> unify_one indent (Meta mv1') (Meta mv2'))
    | Meta { instance = Link mv; _ }, t ->
      if show_unify_logs
      then
        printf
          "%smeta with link instance = %s\n"
          (indent_str (indent + 1))
          (show (Mono (Meta mv)));
      unify_one (indent + 1) (Meta mv) t
    | Meta { instance = Just inst; _ }, t ->
      if show_unify_logs
      then
        printf
          "%smeta with just instance = %s\n"
          (indent_str (indent + 1))
          (show (Mono inst));
      unify_one (indent + 1) inst t
    | Meta ({ instance = Undefined; _ } as mv), t ->
      if occurrence_check mv t
      then raise (Unification_error "Infinite type detected")
      else (
        if show_unify_logs
        then printf "%s<== %s := %s   (mv, t)\n" (indent_str indent) (show pt1) (show pt2);
        mv.instance <- Just t)
    | t, Meta mv ->
      if show_unify_logs
      then printf "%s==> %s := %s   (t, mv)\n" (indent_str indent) (show pt1) (show pt2);
      unify_one indent (Meta mv) t
    | Unknown, _ -> ()
    | _, Unknown -> ()
    | _ ->
      if show_unify_logs then flush stdout;
      raise
        (Unification_error ("Cannot unify " ^ show (Mono t1) ^ " with " ^ show (Mono t2)))
  and unify_many (indent : int) (t1s : mono list) (t2s : mono list) : unit =
    (t1s, t2s)
    |> Tuple.map List.to_seq
    |> Tuple.uncurry Seq.zip
    |> Seq.map (Tuple.uncurry (unify_one indent))
    |> List.of_seq
    |> ignore
  in
  let result = unify_one 0 t1 t2 in
  if show_unify_logs
  then printf "<== unified '%s'\n       with '%s'\n" (show (Mono t1)) (show (Mono t2));
  result
;;

[@@@ocaml.warning "-32"]

(** Reinstantiates unknowns to fresh meta variables. This may lead to unexpected results,
    * so it is not used currently. *)
let abstract_over_unknowns (t : Cs_type.mono) : Cs_type.t =
  let rec aux_one (acc : string list * Cs_type.mono)
    : Cs_type.mono -> string list * Cs_type.mono
    =
    let names, _ = acc in
    function
    | Unknown ->
      let index = List.length names in
      ("t" ^ string_of_int index) :: names, Bound index
    | Stack s ->
      let names', ts = List.fold_left aux_list (names, []) s in
      names', Stack ts
    | Map (ks, vs) ->
      let f = List.fold_left aux_list in
      let names, ks = f (names, []) ks in
      let names, vs = f (names, []) vs in
      names, Map (ks, vs)
    | Fun (is, os) ->
      let f = List.fold_left aux_list in
      let names, is = f (names, []) is in
      let names, os = f (names, []) os in
      names, Fun (is, os)
    | _ -> acc
  and aux_list (acc : string list * Cs_type.mono list) (t : Cs_type.mono)
    : string list * Cs_type.mono list
    =
    let names, ts = acc in
    let names, t' = aux_one (names, t) t in
    names, t' :: ts
  in
  let names, mt = aux_one ([], t) t in
  Poly (names, mt)
;;

(** Creates a return type list of the form [ t1 t2 ... tn ] where n is the length of
    * the output stack. Used in function applications to unify with the applied function. *)
let make_ret_type (n : int) : string list * Cs_type.mono list =
  let (names, levels) : string Seq.t * int Seq.t =
    Seq2.nats0
    |> Seq.map (fun level -> "t" ^ string_of_int level, level)
    |> Seq.take n
    |> Seq.unzip
  in
  ( names |> List.of_seq
  , levels |> List.of_seq |> List.rev |> List.map (fun index -> Cs_type.Bound index) )
;;

exception Underapplied_function of int * int

(** Infers the type of an application, i.e. the behaviour of the type stack applied
    * to the given function. *)
let infer_application
  (top_of_stack : Cs_type.mono list)
  (arity : int)
  (os_arity : int)
  (ty : Cs_type.t)
  : Cs_type.mono list
  =
  let tos_len = List.length top_of_stack in
  if tos_len < arity then raise (Underapplied_function (arity, tos_len));
  let instantiated_fun = instantiate ty in
  let tos_fun =
    let names, ret = make_ret_type os_arity in
    Cs_type.Poly (names, Fun (top_of_stack, ret)) |> instantiate
  in
  if show_inference_logs
  then
    Printf.printf
      "infer app with tos fun = %s, fun = %s\n"
      (Cs_type.show (Mono tos_fun))
      (Cs_type.show (Mono instantiated_fun));
  (* TODO: Maybe this is useful, but it is not clear yet.
   * It did not bring the desired results so far *)
  (* let instantiated_fun = abstract_over_unknowns instantiated_fun |> instantiate in
     Printf.printf "------------------------- abstracted fun = %s\n" (Cs_type.show (Mono instantiated_fun)); *)
  unify instantiated_fun tos_fun;
  match Cs_type.remove_metas instantiated_fun with
  | Fun (in_stack, out_stack) as f ->
    (* The out stack needs to be reversed because it expects the top to be right and not left. *)
    if show_inference_logs then Printf.printf "inst fun = %s\n" (Cs_type.show (Mono f));
    out_stack @ List2.drop (List.length in_stack) top_of_stack
  | _ -> failwith "unreachable"
;;

let unknowns : Cs_type.mono Seq.t = Seq.repeat Cs_type.Unknown

(** Infers the type of a quotation, i.e. the type of a potentially under-applied
    * function. Thus, it cannot be the same as application inference where a not fully
    * applied function causes an error which is undesirable here. *)
let rec infer_quotation (dict : dictionary) (quot : item list) : Cs_type.mono =
  if show_inference_logs
  then Printf.printf "infer quotation of %s\n" (show_item (Stack quot));
  let under_applied_arguments_count : int ref = ref 0 in
  let aux (acc : Cs_type.mono list * int) (item : item) : Cs_type.mono list * int =
    let acc_stack, depth = acc in
    if show_inference_logs
    then Printf.printf "acc stack = %s\n" (Cs_type.show (Mono (Cs_type.Stack acc_stack)));
    match item with
    | Stack s ->
      if show_inference_logs then Printf.printf "infer nested quotation\n==>\n";
      let quot = infer_quotation dict s in
      if show_inference_logs
      then
        Printf.printf
          "<== stack intermediate infer quotation result: %s\n"
          (Cs_type.show (Mono quot));
      quot :: acc_stack, depth
    | item ->
      let item_type : Cs_type.t = infer_item_type dict item in
      if show_inference_logs
      then Printf.printf "item type = %s\n" (Cs_type.show item_type);
      if List.exists
           (function
             | Cs_type.Bound _ -> true
             | _ -> false)
           acc_stack
      then failwith "sanity check failure: acc_os has bound variables";
      (match Cs_type.get_fun_signature item_type with
       | Some (is, os) ->
         let is_len, os_len = List.length is, List.length os in
         let diff = is_len - List.length acc_stack in
         let tos =
           if diff > 0
           then (
             if show_inference_logs
             then Printf.printf "Fill TOS up with %d unknowns\n" diff;
             under_applied_arguments_count := !under_applied_arguments_count + diff;
             (* Append the unknowns because they are at the bottom of the TOS. *)
             acc_stack @ (unknowns |> Seq.take diff |> List.of_seq))
           else acc_stack
         in
         let tos_fun_in = if diff < 0 then List2.take tos is_len else tos in
         if show_inference_logs
         then Printf.printf "TOS = %s\n" (Cs_type.show (Mono (Stack tos)));
         let names, levels =
           Seq2.nats0
           |> Seq.map (fun level -> "t" ^ string_of_int level, level)
           |> Seq.take os_len
           |> Seq.unzip
         in
         let tos_fun : Cs_type.mono =
           Cs_type.Poly
             ( names |> List.of_seq
             , Fun
                 ( tos_fun_in
                 , levels
                   |> List.of_seq
                   |> List.rev
                   |> List.map (fun index -> Cs_type.Bound index) ) )
           |> instantiate
         in
         let instantiated_fun = item_type |> instantiate in
         if show_inference_logs
         then
           Printf.printf
             "tos fun = %s\ninst fun = %s\n"
             (Cs_type.show (Mono tos_fun))
             (Cs_type.show (Mono instantiated_fun));
         unify tos_fun instantiated_fun;
         let acc_stack', depth =
           match instantiated_fun with
           | Fun (_, os) ->
             let os' = os |> List.map Cs_type.remove_metas in
             let acc_stack' =
               acc_stack
               |> List.map Cs_type.remove_metas
               |> List.to_seq
               |> Seq.drop is_len
               |> List.of_seq
               |> ( @ ) os'
             in
             if show_inference_logs
             then
               Printf.printf
                 "after unify: quot type = %s\n"
                 (Cs_type.show (Mono (Cs_type.Fun (tos, acc_stack'))));
             acc_stack', Int.max depth is_len
           | _ -> failwith "unreachable: instantiated function is not a function"
         in
         if show_inference_logs
         then
           Printf.printf
             "intermediate infer quotation result: %s\n"
             (Cs_type.show (Mono (Stack acc_stack')));
         acc_stack', depth
       | None ->
         (match item_type with
          | Mono t ->
            let acc_stack' = t :: acc_stack in
            if show_inference_logs
            then
              Printf.printf
                "intermediate infer quotation result: %s\n"
                (Cs_type.show (Mono (Stack acc_stack')));
            acc_stack', depth
          | _ ->
            failwith
              "unreachable: infer_quotation got a poly type that is not a function."))
  in
  let acc_stack, _depth = quot |> List.fold_left aux ([], 0) in
  let result =
    if !under_applied_arguments_count > 0
    then
      Cs_type.Fun
        (unknowns |> Seq.take !under_applied_arguments_count |> List.of_seq, acc_stack)
    else Stack acc_stack
  in
  if show_inference_logs
  then Printf.printf "infer quotation result: %s\n" (Cs_type.show (Mono result));
  result

(** Infers the type of a single item. *)
and infer_item_type (dict : dictionary) : item -> Cs_type.t = function
  | Word w when String2.is_integer w -> Mono (Cs_type.Free "Int")
  | Word w ->
    (match Hashtbl.find_opt dict (Word w) with
     | Some (Fun (_, ty)) -> ty
     | Some item -> infer_item_type dict item
     | None -> Mono (Cs_type.Free "Wrd"))
  | Stack s ->
    (* TODO: Someday, support poly types? => Rank-N polymorphism, but it seems necessary. *)
    Mono (infer_quotation dict s)
  | Map dict ->
    (* TODO: converting this to stacks may cause problems because it could try to infer
     * the stack effect instead of pushing the data. *)
    let aux f = f dict |> List.of_seq |> (fun s -> Stack s) |> infer_item_type dict in
    let key_types = aux Hashtbl.to_seq_keys in
    let value_types = aux Hashtbl.to_seq_values in
    (match key_types, value_types with
     | Mono (Stack ks), Mono (Stack vs) -> Mono (Cs_type.Map (ks, vs))
     | _ -> raise (Failure "unreachable"))
  | Fun (_, ty) -> ty
  | Type t -> t
  | Nil -> Mono (Cs_type.Free "Nil")
;;

(** The internal inference function that simulates the behaviour of the type stack
    * for a given quotation. Since the quotation is evaluated from left to right, this
    * is essentially a fold left, inferring the current item's type and applying it
    * onto the type stack. *)
let infer_with : stack -> stack option = function
  | Stack quot :: Stack type_stack :: Map dict :: rds ->
    let infer_step (type_stack : item list) (item : item) : item list =
      if show_inference_logs
      then Printf.printf "infer step with ts = %s\n" (show_item (Stack type_stack));
      let aux : item -> ((int * int) * Cs_type.t) option = function
        | Word w ->
          if show_inference_logs then Printf.printf "infer type of word %s\n" w;
          let* ty =
            Hashtbl.find_opt dict (Word w)
            |> Option.map (fun item -> infer_item_type dict item)
          in
          if show_inference_logs
          then Printf.printf "inferred type = %s\n" (Cs_type.show ty);
          let* is, os = Cs_type.get_fun_signature ty in
          Some ((List.length is, List.length os), ty)
        | Fun (_, ty) ->
          if show_inference_logs then Printf.printf "item is function\n";
          let* is, os = Cs_type.get_fun_signature ty in
          Some ((List.length is, List.length os), ty)
        | _ -> None
      in
      if show_inference_logs
      then Printf.printf "infer step for item %s\n" (show_item item);
      match aux item with
      | Some ((arity, out_arity), ty) ->
        let as_types : item -> Cs_type.mono = function
          | Type (Mono t) -> t
          | _ -> raise (Failure "unreachable")
        in
        let top_of_stack : Cs_type.mono list =
          List2.take type_stack arity |> List.map as_types
        in
        let rest_stack : item list = List2.drop arity type_stack in
        if show_inference_logs
        then
          Printf.printf
            "infer application for type %s with arity %d and top of stack = %s\n"
            (Cs_type.show ty)
            arity
            (Cs_type.show (Mono (Stack top_of_stack)));
        (infer_application top_of_stack arity out_arity ty
         |> List.map (fun t -> Type (Mono t)))
        @ rest_stack
      | None ->
        let t = infer_item_type dict item in
        if show_inference_logs then Printf.printf "push type %s\n" (Cs_type.show t);
        Type t :: type_stack
    in
    (try
       let new_type_stack = List.fold_left infer_step type_stack quot in
       Some (Stack new_type_stack :: rds)
     with
     | Unification_error msg ->
       if show_unify_logs then Printf.printf "type error: %s\n" msg;
       Some (Stack [ Word "type error"; Word msg ] :: rds)
     | Underapplied_function (arity, tos_len) ->
       Some
         (Word
            ("Under applied function error: expected "
             ^ string_of_int arity
             ^ " arguments but got "
             ^ string_of_int tos_len
             ^ ".")
          :: rds))
  | _ -> None
;;

(** Infers a quotation, starting with an empty type stack. See `infer_with`. *)
let infer : stack -> stack option = function
  | cs :: Stack (Stack quot :: rds) :: Map dict :: rest_meta ->
    infer_with (Stack quot :: Stack [] :: Map dict :: rds)
    |> Option.map (fun ds -> cs :: Stack ds :: Map dict :: rest_meta)
  | _ -> None
;;

(** Checks whether a quotation is of the same type as the given one.
    * First, the type of the quotation is inferred and then unified with the expected
    * type. *)
let check : stack -> stack option = function
  | cs :: Stack (Stack expected_type_stack :: Stack quot :: rds) :: Map dict :: rest_meta
    ->
    if List.exists
         (function
           | Type _ -> false
           | _ -> true)
         expected_type_stack
    then failwith "sanity check failure: expected_type_stack has non-types on it";
    (* We need wrap the quotation in another stack to handle under applied definitions, i.e. definitions that expect arguments. *)
    (match infer_with (Stack [ Stack quot ] :: Stack [] :: Map dict :: rds) with
     | Some (Stack [ Type (Mono inferred_type) ] :: rds) ->
       (* Currently, inferred functions need to be wrapped into a stack because the quotation inference
        * does _not_ wrap inferred functions in a stack, unlike for every other type because functions
        * are not in a stack per se. *)
       let inferred_type =
         match inferred_type with
         | Fun _ as f -> Cs_type.Stack [ f ]
         | t -> t
       in
       let ets =
         Cs_type.Stack
           (expected_type_stack
            |> List.map (function
              | Type t -> instantiate t
              | unexpected ->
                failwith
                  ("unreachable: got non-type in expected type stack of check and \
                    instead "
                   ^ show_item unexpected)))
       in
       (try
          unify ets inferred_type;
          Some (cs :: Stack (Word "t" :: rds) :: Map dict :: rest_meta)
        with
        | Unification_error msg ->
          Some
            (cs
             :: Stack (Stack [ Word ("\"" ^ msg ^ "\""); Word "type error:" ] :: rds)
             :: Map dict
             :: rest_meta))
     | Some (unexpected :: _) ->
       failwith
         ("unreachable: infer_with in check did not return a type item but "
          ^ show_item unexpected)
     | _ -> None)
  | _ -> None
;;

(** Reads a mono type which potentially quantified with the given type list. *)
let read_mono (type_vars : string list) (items : item list) : Cs_type.mono option =
  let is_fun_sep : item -> bool = function
    | Word "--" -> true
    | _ -> false
  in
  let rec read_one (type_vars : string list) : item -> Cs_type.mono option = function
    | Word "?" -> Some Unknown
    | Word w ->
      (match List.find_index (fun type_var -> type_var = w) type_vars with
       | Some index -> Some (Bound index)
       | None -> Some (Free w))
    | Stack items ->
      if List.exists is_fun_sep items
      then read_function type_vars items
      else read_many type_vars items |> Option.map (fun types -> Cs_type.Stack types)
    | Map dict ->
      (match Hashtbl.to_seq dict |> List.of_seq with
       | [ (Stack key_types, Stack value_types) ] ->
         let* ktys = read_many type_vars key_types in
         let* vtys = read_many type_vars value_types in
         Some (Cs_type.Map (ktys, vtys))
       | _ -> None)
    | _ -> None
  and read_many (type_vars : string list) (items : item list) : Cs_type.mono list option =
    List.map (read_one type_vars) items |> Option2.sequence
  and read_function (type_vars : string list) (items : item list) : Cs_type.mono option =
    let* (in_stack, out_stack) : Cs_type.mono list * Cs_type.mono list =
      List2.split_when items is_fun_sep
      |> Tuple.map (read_many type_vars)
      |> Option2.tuple_sequence
    in
    Some (Cs_type.Fun (List.rev in_stack, List.rev out_stack))
  in
  (* TODO: What to do with the remaining items in the else case? *)
  if List.exists is_fun_sep items
  then read_function type_vars items
  else read_one type_vars (List.hd items)
;;

(** Reads a poly type which, i.e. the type variables are collected and the
    * nested mono type is read with them. *)
let read_poly (items : item list) : Cs_type.t option =
  let is_dot : item -> bool = function
    | Word "." -> true
    | _ -> false
  in
  let type_var_items, mono_type = List2.split_when items is_dot in
  let make_type_var : item -> string option = function
    | Word w -> Some w
    | _ -> None
  in
  let* (type_vars : string list) =
    List.map make_type_var type_var_items |> Option2.sequence
  in
  let* ty = read_mono type_vars mono_type in
  Some (Cs_type.Poly (type_vars, ty))
;;

(** Reads a generic type from the top element of the data stack. *)
let read_type : stack -> stack option = function
  | Stack (Word "forall" :: rest) :: rds ->
    read_poly rest |> Option.map (fun t -> Type t :: rds)
  | Stack (Word "∀" :: rest) :: rds ->
    read_poly rest |> Option.map (fun t -> Type t :: rds)
  | Stack s :: rds -> read_mono [] s |> Option.map (fun t -> Type (Cs_type.Mono t) :: rds)
  | (Word _ as w) :: rds ->
    read_mono [] [ w ] |> Option.map (fun t -> Type (Cs_type.Mono t) :: rds)
  | _ -> None
;;

(** Shows the type of the top element of the data stack. *)
let show_type : stack -> stack option = function
  | Type ty :: rds -> Some (Word (Cs_type.show ty) :: rds)
  | _ -> None
;;

(** Shows the type information, i.e. what type it is, of the top element of the
    * data stack. *)
let type_info : stack -> stack option =
  let aux : Cs_type.mono -> string = function
    | Unknown -> "unknown"
    | Free name -> "free(" ^ name ^ ")"
    | Bound index -> "bound(" ^ string_of_int index ^ ")"
    | Stack s -> "stack(len = " ^ string_of_int (List.length s) ^ ")"
    | Map _ -> "map"
    | Fun _ -> "function"
    | Meta _ -> "meta"
  in
  function
  | Type (Mono t) :: rds -> Some (Word (aux t) :: rds)
  | Type (Poly (_, t)) :: rds -> Some (Word (aux t) :: rds)
  | _ -> None
;;

(** Prints the name of the word its type if it is in the dictionary. *)
let source : stack -> stack option = function
  | Stack cs :: Stack (item :: rds) :: Map dict :: rest ->
    (match Hashtbl.find_opt dict item with
     | Some _ ->
       let item_type = infer_item_type dict item in
       let type_word : string = Cs_type.show item_type in
       let ds' : item list =
         Stack [ item; Word " [ "; Word type_word; Word " ]\n" ] :: rds
       in
       Some (Stack (Word "word" :: Word "print" :: cs) :: Stack ds' :: Map dict :: rest)
     | _ -> None)
  | _ -> None
;;
