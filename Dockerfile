FROM debian

WORKDIR /app

COPY . .

RUN apt update && apt install -y opam
RUN opam init --compiler=5.1.0 -y
RUN eval $(opam env --safe)
RUN opam install dune
RUN eval $(opam env --safe)
