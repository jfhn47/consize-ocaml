(** Consize Type *)

type mono =
  | Unknown
  | Free of string
  | Bound of int
  (* Uses De-Bruijn indices. TODO: Would it make the entire thing easier if we switch to levels? *)
  | Stack of mono list
  | Map of mono list * mono list (* { [ key-types ] [ value-types ] } *)
  | Fun of mono list * mono list
  | Meta of meta_var

and meta_var_instance =
  | Link of meta_var
  | Just of mono
  | Undefined

and meta_var =
  { mutable instance : meta_var_instance
  ; name : string
  ; id : int
  }

val fresh_meta_var : string -> meta_var
val prune_meta : meta_var -> meta_var
val resolve_meta : meta_var -> mono

(** Allow quantified types only a the top level => no rank n polymorphism. *)
type t =
  | Mono of mono
  | Poly of string list * mono
(* forall [a..z]. t [a..z], the name is just for printing => locally nameless *)

val mono_equals : mono * mono -> bool
val get_fun_signature : t -> (mono list * mono list) option
val show : t -> string
val show_debug : t -> string
val remove_metas : mono -> mono
