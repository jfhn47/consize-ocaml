open Util

type mono =
  | Unknown
  | Free of string
  | Bound of int
  | Stack of mono list
  | Map of mono list * mono list
  | Fun of mono list * mono list
  | Meta of meta_var

and meta_var_instance =
  | Link of meta_var
  | Just of mono
  | Undefined

and meta_var =
  { mutable instance : meta_var_instance
  ; name : string
  ; id : int
  }

type t =
  | Mono of mono
  | Poly of string list * mono

let next_meta_var_id : int ref = ref 0

let fresh_meta_var (name : string) : meta_var =
  let id = !next_meta_var_id in
  next_meta_var_id := !next_meta_var_id + 1;
  { instance = Undefined; name; id }
;;

let rec prune_meta (mv : meta_var) : meta_var =
  match mv.instance with
  | Link mv' ->
    mv.instance <- Link (prune_meta mv');
    mv'
  | _ -> mv
;;

let resolve_meta (mv : meta_var) : mono =
  match prune_meta mv with
  | { instance = Just t; _ } -> t
  | _ -> Meta mv
;;

type context =
  { names : (string, int) Hashtbl.t
  ; indices : string list
  }

let rec mono_equals : mono * mono -> bool = function
  | Unknown, Unknown -> true
  | Free s, Free s' -> s = s'
  | Bound i, Bound i' -> i = i'
  | Stack types, Stack types' ->
    let len = List.length types in
    len = List.length types'
    && (len = 0 || List.for_all2 (Tuple.curry mono_equals) types types')
  | Map (keys, values), Map (keys', values') ->
    mono_equals (Stack keys, Stack keys') && mono_equals (Stack values, Stack values')
  | Fun (input, output), Fun (input', output') ->
    mono_equals (Stack input, Stack input') && mono_equals (Stack output, Stack output')
  | _ -> false
;;

let get_fun_signature : t -> (mono list * mono list) option =
  let aux : mono -> (mono list * mono list) option = function
    | Fun (in_stack, out_stack) -> Some (in_stack, out_stack)
    | _ -> None
  in
  function
  | Poly (_, mono) -> aux mono
  | Mono mono -> aux mono
;;

let show : t -> string =
  let rec show_mono ?(already_embraced = false) (ctx : context) : mono -> string =
    let indices_count = List.length ctx.indices in
    function
    | Unknown -> "?"
    | Free name -> name
    | Bound index -> List.nth ctx.indices (indices_count - index - 1)
    | Stack types -> "[ " ^ String2.concat " " (List.map (show_mono ctx) types) ^ " ]"
    | Map (keys, values) ->
      "{ [ "
      ^ String2.concat " " (List.map (show_mono ctx) keys)
      ^ " ] [ "
      ^ String2.concat " " (List.map (show_mono ctx) values)
      ^ " ] }"
    | Fun (input, output) ->
      let show_types : mono list -> string list =
        List.map (show_mono ~already_embraced:true ctx)
      in
      let prefix, postfix = if already_embraced then "", "" else "[ ", " ]" in
      let istr =
        match input with
        | [] -> ""
        | input -> (input |> List.rev |> show_types |> String2.concat " ") ^ " "
      in
      let ostr =
        match output with
        | [] -> ""
        | input -> " " ^ (input |> List.rev |> show_types |> String2.concat " ")
      in
      prefix ^ istr ^ "--" ^ ostr ^ postfix
    | Meta mvar ->
      let name = mvar.name ^ "?" in
      (match mvar.instance with
       | Link mv -> "(" ^ name ^ " := " ^ show_mono ctx (Meta mv) ^ ")"
       | Just m -> "(" ^ name ^ " := " ^ show_mono ctx m ^ ")"
       | Undefined -> name)
  in
  function
  | Mono mono -> show_mono { names = Hashtbl.create 0; indices = [] } mono
  | Poly (type_vars, body) ->
    let names =
      type_vars |> List.rev |> List.to_seq |> Seq.zip Seq2.nats0 |> Seq.map Tuple.swap
    in
    let context = { names = Hashtbl.of_seq names; indices = List.rev type_vars } in
    "[ forall "
    ^ String2.concat " " type_vars
    ^ " . "
    ^ show_mono ~already_embraced:true context body
    ^ " ]"
;;

let show_debug : t -> string =
  let rec show_mono ?(already_embraced = false) (ctx : context) : mono -> string =
    let indices_count = List.length ctx.indices in
    function
    | Unknown -> "?"
    | Free name -> "Tfree(" ^ name ^ ")"
    | Bound index -> "TBound(" ^ List.nth ctx.indices (indices_count - index - 1) ^ ")"
    | Stack types ->
      "Tstack[ " ^ String2.concat " " (List.map (show_mono ctx) types) ^ " ]"
    | Map (keys, values) ->
      "Tmap{ [ "
      ^ String2.concat " " (List.map (show_mono ctx) keys)
      ^ " ] [ "
      ^ String2.concat " " (List.map (show_mono ctx) values)
      ^ " ] }"
    | Fun (input, output) ->
      let show_types : mono list -> string list =
        List.map (show_mono ~already_embraced:true ctx)
      in
      let prefix, postfix =
        if already_embraced then "Tfunction(", ")" else "Tfunction[ ", " ]"
      in
      let istr =
        match input with
        | [] -> ""
        | input -> (input |> List.rev |> show_types |> String2.concat " ") ^ " "
      in
      let ostr =
        match output with
        | [] -> ""
        | input -> " " ^ (input |> List.rev |> show_types |> String2.concat " ")
      in
      prefix ^ istr ^ "--" ^ ostr ^ postfix
    | Meta mvar ->
      let name = mvar.name ^ "?" in
      (match mvar.instance with
       | Link mv -> "(" ^ name ^ " := " ^ show_mono ctx (Meta mv) ^ ")"
       | Just m -> "(" ^ name ^ " := " ^ show_mono ctx m ^ ")"
       | Undefined -> name)
  in
  function
  | Mono mono -> show_mono { names = Hashtbl.create 0; indices = [] } mono
  | Poly (type_vars, body) ->
    let names =
      type_vars |> List.rev |> List.to_seq |> Seq.zip Seq2.nats0 |> Seq.map Tuple.swap
    in
    let context = { names = Hashtbl.of_seq names; indices = List.rev type_vars } in
    "[ forall "
    ^ String2.concat " " type_vars
    ^ " . "
    ^ show_mono ~already_embraced:true context body
    ^ " ]"
;;

let rec remove_metas : mono -> mono = function
  | Meta mv ->
    (match resolve_meta mv with
     | Meta _ -> Unknown
     (* raise (Failure ("Unification error: unresolved meta variable " ^ show (Mono (Meta mv)))) *)
     | t -> t)
  | Stack s -> Stack (List.map remove_metas s)
  | Map (xs, ys) -> Map (List.map remove_metas xs, List.map remove_metas ys)
  | Fun (xs, ys) -> Fun (List.map remove_metas xs, List.map remove_metas ys)
  | t -> t
;;
