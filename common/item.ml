type item =
  | Word of string
  | Stack of stack
  | Map of dictionary
  | Fun of (stack -> stack option) * Cs_type.t
  | Type of Cs_type.t
  | Nil

and stack = item list
and dictionary = (item, item) Hashtbl.t

open Util

let rec show_item (item : item) : string =
  match item with
  | Word word -> word
  | Stack items -> show_stack ~with_brackets:true items
  | Map dict ->
    let show_entry : item * item -> string = function
      | key, value -> show_item key ^ " " ^ show_item value
    in
    let content =
      dict |> Hashtbl.to_seq |> Seq.map show_entry |> List.of_seq |> String2.concat " "
    in
    ignore content;
    "{ " ^ content ^ " }"
  | Fun _ -> "<fct>"
  | Type t -> Cs_type.show t
  | Nil -> "nil"

and debug_show_item : item -> string = function
  | Word word -> "(Word '" ^ word ^ "')"
  | Stack items -> "(Stack " ^ debug_show_stack items ^ ")"
  | Map _ -> "Map ..."
  | Fun _ -> "<fct>"
  | Type t -> "(Type " ^ Cs_type.show_debug t ^ ")"
  | Nil -> "nil"

and show_stack ?(with_brackets = false) (stack : stack) : string =
  let f acc item = acc ^ show_item item ^ " " in
  let prefix, postfix = if with_brackets then "[ ", "]" else "", "" in
  List.fold_left f prefix (if with_brackets then stack else List.rev stack) ^ postfix

and debug_show_stack (stack : stack) : string =
  let f acc item = acc ^ debug_show_item item ^ " " in
  List.fold_left f "[ " stack ^ "]"
;;
