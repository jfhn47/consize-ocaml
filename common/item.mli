(** Items can be words, stacks, maps, functions, types or nil.
    * Functions may fail, so the map a stack to an optional stack where
    * `None` indicates a failure. *)
type item =
  | Word of string
  | Stack of stack
  | Map of dictionary
  | Fun of (stack -> stack option) * Cs_type.t
  | Type of Cs_type.t
  | Nil

and stack = item list
and dictionary = (item, item) Hashtbl.t

val show_item : item -> string
val debug_show_item : item -> string
