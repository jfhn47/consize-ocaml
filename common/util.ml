(* We have to use different names for the modules because OCaml cannot extend already defined modules. *)

module List2 = struct
  (* Reference: https://github.com/janestreet/base/blob/master/src/list.ml, line 1379 *)
  let take (lst : 'a list) (count : int) : 'a list =
    if count <= 0
    then []
    else (
      let rec aux n rest acc =
        match rest with
        | [] -> lst
        | x :: xs -> if n = 0 then List.rev acc else aux (n - 1) xs (x :: acc)
      in
      aux count lst [])
  ;;

  let split_when (list : 'a list) (pred : 'a -> bool) : 'a list * 'a list =
    let rec aux (rest : 'a list) (acc : 'a list) : 'a list * 'a list =
      match rest with
      | [] -> list, []
      | x :: xs -> if pred x then List.rev acc, xs else aux xs (x :: acc)
    in
    aux list []
  ;;

  let rec drop (n : int) (list : 'a list) : 'a list =
    if n <= 0 then list else drop (n - 1) (List.tl list)
  ;;

  let rec drop_while (list : 'a list) (pred : 'a -> bool) : 'a list =
    match list with
    | [] -> []
    | x :: xs -> if pred x then drop_while xs pred else x :: xs
  ;;

  let snoc_hd_opt (lst : 'a list) : ('a list * 'a) option =
    let len = List.length lst in
    if len > 0 then Some (take lst (len - 1), List.nth lst (len - 1)) else None
  ;;
end

module String2 = struct
  (* Source: https://caml.inria.fr/pub/old_caml_site/FAQ/FAQ_EXPERT-eng.html#strings *)
  let explode (s : string) : char list =
    let rec aux (index : int) (acc : char list) : char list =
      if index < 0 then acc else aux (index - 1) (s.[index] :: acc)
    in
    aux (String.length s - 1) []
  ;;

  (* Source: https://caml.inria.fr/pub/old_caml_site/FAQ/FAQ_EXPERT-eng.html#strings *)
  let implode (chars : char list) : string =
    let res = Bytes.create (List.length chars) in
    let rec aux (index : int) = function
      | [] -> res
      | c :: cs ->
        Bytes.set res index c;
        aux (index + 1) cs
    in
    aux 0 chars |> Bytes.to_string
  ;;

  let split_on_chars (s : string) (delims : char list) : string list =
    let chars = explode s in
    let rec aux (acc : string list) (current : char list) (input : char list)
      : string list
      =
      match input with
      | [] -> List.rev ((List.rev current |> implode) :: acc)
      | c :: cs ->
        if List.mem c delims
        then (
          match current with
          | [] -> aux acc [] cs
          | other -> aux ((List.rev other |> implode) :: acc) [] cs)
        else aux acc (c :: current) cs
    in
    aux [] [] chars
  ;;

  (* For whatever reason, the standard library function is unbound. *)
  let concat (seperator : string) (items : string list) : string =
    match List2.snoc_hd_opt items with
    | Some (init, last) ->
      (match init with
       | head :: init ->
         let f acc x = acc ^ seperator ^ x in
         head ^ List.fold_left f "" init ^ seperator ^ last
       | _ -> last)
    | None -> ""
  ;;

  let is_integer (s : string) : bool =
    try
      int_of_string s |> ignore;
      true
    with
    | Failure _ -> false
  ;;
end

module Char2 = struct
  let to_string (c : char) : string = Stdlib.String.make 1 c
end

module Tuple = struct
  let map (f : 'a -> 'b) = function
    | a, b -> f a, f b
  ;;

  let map_second (f : 'a -> 'b) : 'c * 'a -> 'c * 'b = function
    | a, b -> a, f b
  ;;

  let map_second2 (f : 'a -> 'b) : 'c * 'a -> 'b = function
    | _, b -> f b
  ;;

  let first : 'a * 'b -> 'a = function
    | a, _ -> a
  ;;

  let second : 'a * 'b -> 'b = function
    | _, b -> b
  ;;

  let swap : 'a * 'b -> 'b * 'a = function
    | a, b -> b, a
  ;;

  let uncurry (f : 'a -> 'b -> 'c) : 'a * 'b -> 'c = function
    | a, b -> f a b
  ;;

  let curry (f : 'a * 'b -> 'c) (a : 'a) (b : 'b) : 'c = f (a, b)
end

module Option2 = struct
  let ( let* ) = Option.bind

  (* Similar to the Haskell function sequence. *)
  let rec sequence : 'a option list -> 'a list option = function
    | [] -> Some []
    | Some x :: xs ->
      let* xs' = sequence xs in
      Some (x :: xs')
    | None :: _ -> None
  ;;

  let seq_sequence (seq : 'a option Seq.t) : 'a Seq.t option =
    let rec aux (seq : 'a option Seq.t) : 'a Seq.t option =
      match Seq.uncons seq with
      | None -> Some Seq.empty
      | Some (Some x, xs) ->
        let* xs' = aux xs in
        Some (Seq.cons x xs')
      | Some (None, _) -> None
    in
    aux seq
  ;;

  let tuple_sequence : 'a option * 'b option -> ('a * 'b) option = function
    | Some a, Some b -> Some (a, b)
    | _ -> None
  ;;
end

module Seq2 = struct
  let nats0 : int Seq.t = Seq.unfold (fun n -> Some (n, n + 1)) 0
end

module Either2 = struct
  (* Either is monadic in its right type. *)
  let bind (a : ('a, 'b) Either.t) (f : 'b -> ('a, 'c) Either.t) : ('a, 'c) Either.t =
    match a with
    | Left a -> Left a
    | Right b -> f b
  ;;
end

(* Source: https://v2.ocaml.org/manual/bindingops.html *)
module type Applicative_syntax = sig
  type 'a t

  val ( let+ ) : 'a t -> ('a -> 'b) -> 'b t
  val ( and+ ) : 'a t -> 'b t -> ('a * 'b) t
end

module type Monad_syntax = sig
  include Applicative_syntax

  val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
  val ( and* ) : 'a t -> 'b t -> ('a * 'b) t
end

let compose (f : 'b -> 'c) (g : 'a -> 'b) (x : 'a) : 'c = f (g x)
let ( @$ ) = compose

exception Todo

let todo () = raise Todo
