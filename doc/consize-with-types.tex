%!TeX program = xelatex

\documentclass{scrartcl}

\usepackage{listings}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{tikz}
\usepackage[libertine]{newtxmath}
\usepackage[no-math]{fontspec}
\usepackage{csquotes}
\usepackage{todonotes}
\usepackage{setspace}
\usepackage[ngerman]{babel}
\usepackage{xcolor}
\usepackage[colorlinks=true, linkcolor=blue, urlcolor=blue]{hyperref}
\usepackage[
  backend=biber,
  sorting=ynt,
  style=authoryear,
]{biblatex}
\addbibresource{bib.bib}

\setmainfont{Linux Libertine}
\setsansfont{Linux Libertine}
\setmonofont{Iosevka Fixed}

\NewDocumentCommand{\inferencerule}{m m o}{%
  \dfrac{\; #1 \;}{\; #2 \;}%
  \IfValueT{#3}{\; [\text{#3}]}
}

\newcommand{\mathlstinline}[1]{\text{\lstinline|#1|}}
\newcommand{\stackcons}{\bullet}
% Take from: https://tex.stackexchange.com/a/272758
\DeclareMathOperator{\doubleplus}{+\kern -0.4em+}
\newcommand{\stackconcat}{\doubleplus}

\newcommand{\codebeginbracket}{%
  \noindent%
  \begin{tikzpicture}[remember picture, overlay]
    \draw[very thick] (-2mm,0) -- (\textwidth+2mm,0);
    \draw[thick] (-2mm,0.21mm)   -- (-2.2mm,-2mm);
    \draw[thick] (-1.8mm,0.10mm) -- (-2.2mm,-2mm);
    \draw[thick] (\textwidth+2mm,0.21mm)   -- (\textwidth+2.2mm,-2mm);
    \draw[thick] (\textwidth+1.8mm,0.10mm) -- (\textwidth+2.2mm,-2mm);
  \end{tikzpicture}%
  \vspace*{-0.5mm}
}

\newcommand{\codeendbracket}{%
  \vspace*{-3mm}%
  \noindent%
  \begin{tikzpicture}[remember picture, overlay]
    \draw[very thick] (-2mm,0) -- (\textwidth+2mm,0);
    \draw[thick] (-2mm,-0.21mm)   -- (-2.2mm,2mm);
    \draw[thick] (-1.8mm,0.10mm) -- (-2.2mm,2mm);
    \draw[thick] (\textwidth+2mm,-0.21mm)   -- (\textwidth+2.2mm,2mm);
    \draw[thick] (\textwidth+1.8mm,0.10mm) -- (\textwidth+2.2mm,2mm);
  \end{tikzpicture}%
  \vspace*{2mm}
}

\definecolor{keywordpurple}{HTML}{7F3FA7}
\definecolor{commentgray}{HTML}{AAAAAA}
\definecolor{constructorblue}{HTML}{0C468B}
\definecolor{stringgreen}{HTML}{008000}

\lstdefinelanguage{OCaml}{
  keywords={type, of, module, sig, end, val, let, mutable},
  keywordstyle={\color{keywordpurple}\bfseries},
  emph=[1]{check, infer, read_type, show_type, swap, swap_type, instance, name, id, entries}, % functions
  emphstyle=[1]{\itshape},
  emph=[2]{string, int, mono, list, meta_var, t, stack, option, dictionary, meta_var_instance}, % types
  emphstyle=[2]{\bfseries},
  emph=[3]{Word, Stack, Map, Fun, Type, Nil, Unknown, Free, Bound, Meta, Mono, Poly}, % constuctors
  emphstyle=[3]{\color{constructorblue}},
  morecomment=[s]{(*}{*)},
  morecomment=[n]{(**}{*)},
  commentstyle={\color{commentgray}\bfseries},
  morestring=[b]{"},
  stringstyle={\color{stringgreen}\bfseries},
  showstringspaces=false,
}

\lstset{
  basicstyle=\ttfamily\small,
  basewidth=0.55em,
  breaklines=true,
  numbers=left,
}

\title{Consize erweitert um statische Typisierung\\ \vspace*{3mm} \large \emph{Programmiersprachen, Konzepte und Realisation} --- Ausarbeitung zum Projekt}
\author{Jan Hindges}
\date{\today}

\begin{document}

\maketitle

\begin{center}
  \large \textsc{Abstrakt}
\end{center}

\normalsize

\begin{abstract}
  Programmiersprachen wie \href{https://github.com/denkspuren/consize}{Consize}\footnote{\url{https://github.com/denkspuren/consize}}, \href{https://forth-standard.org/}{Forth}\footnote{\url{https://forth-standard.org/}} oder \href{https://en.wikipedia.org/wiki/Joy_(programming_language)}{Joy}\footnote{\url{https://en.wikipedia.org/wiki/Joy_(programming_language)}} sind konkatenative, Stack-orientierte Programmiersprachen, die darüber hinaus die Abwesenheit einer semantischen Überprüfung der Eingabe gemeinsam haben.
Folglich werden alle syntaktisch korrekten Programme als Eingabe akzeptiert.
Dies schließt ebenfalls semantisch inkorrekte, d.h. in ihrer Bedeutung widersprüchliche bzw. sinnlose, Programme ein, was bei steigender Komplexität zu unerwartetem Verhalten führen kann.
  Die semantische Prüfung von Programmen durch Typsysteme ist ein Ansatz, die Anzahl der akzeptierten Programme möglichst auf sowohl syntaktisch als auch semantisch korrekte zu reduzieren.
  Diese Ausarbeitung beschäftigt sich mit der Erweiterung von Consize um ein Typsystem in der Form von neuen Wörtern, die in bereits bestehende Programme eingefügt werden oder mit denen jene Programme geprüft werden können.
\end{abstract}

\tableofcontents

\newpage

\section{Einleitung und Motivation}

Consize ist eine konkatenative, Stack-orientierte, dynamisch typisierte Programmiersprache, die mit Hilfe eines kleinen Kernels von wenigen vordefinierten \emph{Wörtern} (d.h. Funktionen) ihre Funktionalität erschafft.
Die Sprache bietet ihren eigenen Interpreter, der auch das Programmieren auf Meta-Ebenen erlaubt und damit komplexe Programme ausdrücken kann.
Die potentiellen Probleme von dynamisch typisierten Sprachen zeigen sich jedoch gerade in komplexen Programmen, die für einen Menschen nicht mehr leicht nachvollziehbar sind.
Als Beispiel soll die Funktion \lstinline|zip| dienen, die in der Prelude von Consize wie folgt definiert ist:

\begin{lstlisting}
: zip ( stk1 stk2 -- stk )
  2dup [ empty? ] bi@ or
    [ 2drop ( ) ]
    [ unpush ( ) cons  rot
      unpush rot cons -rot swap zip cons ]
   if ;
\end{lstlisting}

Ohne die Dokumentation des \emph{Stack-Effekts} \lstinline|( stk1 stk2 -- stk )|, müsste die Implementierung der gesamten Funktion erst verstanden werden, um sie korrekt anwenden zu können.
Das Problem des dokumentierten Stack-Effekts ist, dass nicht geprüft wird, ob er zu der Funktion passt und daher kann diese Art der Dokumentation ohne aufzufallen veralten und falsche Informationen vermitteln.
Mit einer ausdrucksstarken Typsignatur, die den Stack-Effekt ausreichend kodiert, kann die Funktion verwendet werden, ohne dass die Implementierung verstanden werden muss oder die Gefahr von falscher Dokumentation entsteht, da die Funktion durch das Typsystem auf semantische Fehler geprüft wurde und nur existieren kann, wenn sie fehlerfrei ist.
Eine Typsignatur dieser Art könnte zum Beispiel für die Funktion \lstinline|swap| wie folgt aussehen: $\forall a \; b. \; a \; b - b \; a$.

Besonderheiten bei der Entwicklung eines Typsystems für eine konkatenative Sprache sind, dass Funktionen immer vollständig angewendet werden, Konzepte wie \emph{currying} müssen daher erst implementiert werden.
Weiterhin unterscheiden sich Funktionen von denen aus funktionalen oder imperativen Sprachen, da kein einzelner Wert berechnet, sondern die \emph{Veränderung} des Stacks (\emph{Stack-Effekt}) berechnet wird.
Daher haben Funktionen immer die Signatur $\alpha_1 \dots \alpha_n - \beta_1 \dots \beta_m$ mit $n, m \in \mathbb{N}_0$ haben.

Die Ausarbeitung kann in der Kürze nicht alle wesentlichen Themen vollumfänglich definieren, daher ist eine Vertrautheit mit Logik oder Typsystem sowie eine oberflächliche Auseinandersetzung mit den Quellen hilfreich.

\section{Theorie}

Dieses Kapitel beschäftigt sich mit dem theoretischen Hintergrund des Typsystems, der Art wie Typsysteme definiert werden und schließlich mit der Definition des hier vorgestellten Typsystems.

\subsection{Syntax, Semantik und induktive definierte Formalismen}

Programmiersprachen sind formale Sprachen, die ihre \emph{Syntax} durch eine \emph{Grammatik} und ihre \emph{Semantik} (Bedeutung) durch die Definition der Evaluation (Berechnung) erhalten [\cite[S.32]{pierce2002types}].
Wenn Ausdrücke einer Sprache so formuliert werden, dass keine Regel der Evaluation zutrifft, ist der Ausdruck \emph{stuck}, was als Laufzeitfehler oder sinnloses Programm interpretiert werden kann [\cite[S.41-42]{pierce2002types}].
Beispielsweise wäre der Ausdruck $1 + \mathit{true}$ in einer Sprache, die Addition nur für Zahlen definiert, \emph{stuck}, da keine Evaluationsregel anwendbar ist [\cite[S.91-92]{pierce2002types}].
Um die Ausführung solcher fehlerhaften Programme zu verhindern, können wir ein \emph{Typsystem} einführen.
Ein Typsystem im Allgemeinen zu beschreiben ist nicht unbedingt einfach, aber Harper [\cite[S.39]{Harper_2016}] liefert eine gute Beschreibung der Aufgaben eines Typsystems:

\begin{quote}
  \enquote{Most programming languages exhibit a \emph{phase distinction} between the \emph{static} and \emph{dynamic} phases of processing.
  The static phase consists of parsing and type checking to ensure that the program is well-formed; the dynamic phase consists of execution of well-formed programs.
  A language is said to be \emph{safe} exactly when well-formed programs are well-behaved when executed.
  [\dots]
  Type safety tells us that these predictions [d.h. das erwartete Programmverhalten] are accurate; if not, the statics is considered to be improperly defined, and the language is deemed \emph{unsafe} for execution.}
\end{quote}

Die statische Logik des Programms, die in der statischen Phase geprüft wird, sorgt damit für die Korrektheit des Programms zur Ausführungszeit.
Diese Logik wird in der Regel durch \emph{operationelle Semantik} [\cite[S.32-34]{pierce2002types}] und \emph{induktive Definitionen} in Form von \emph{Inferenzregeln} beschrieben, die \emph{Urteile} (englisch: \emph{Judgment}) $J$ über \emph{syntaktische Objekte} des Programms fällen.
Die induktiven Definitionen haben die folgende Form:

\[
  \inferencerule{\mathit{Hypothese_1} \qquad \dots \qquad \mathit{Hypothese_n}}{\mathit{Konklusion}}.
\]

\emph{Axiome} sind induktive Definitionen, die keine Hypothesen haben.
Genauer betrachtet ist die Form der induktiven Definitionen:

\[
  \inferencerule{\vec x \vec x_1 \; | \; \Gamma \Gamma_1 \vdash J_1 \qquad \dots \qquad \vec x \vec x_n \; | \; \Gamma \Gamma_n \vdash J_n}{\vec x \; | \; \Gamma \vdash J}.
\]
[\cite[S.15-16,34-35]{Harper_2016}]

Dabei ist $\Gamma$ eine Liste von wahren Aussagen oder Annahmen, die \emph{Kontext} genannt und als Liste temporärer Axiome betrachtet wird, während $\vec x$ die in der Definitionen vorkommenden Variablen auflistet.
Jede Hypothese hat neben den geteilten Variablen $\vec x$ und Kontext $\Gamma$ ihren eigenen Kontext $\Gamma_i$ sowie eigene Variablen $\vec x_i$, die jeweils leer sein können [\cite[S.28,33-35]{Harper_2016}].
Der Ausdruck $\vec x \; | \; \Gamma \vdash J$ bedeutet in etwa \enquote{Für die Variablen $\vec x$ und unter Kontext $\Gamma$ lässt sich Urteil $J$ schlussfolgern.}
In der Praxis werden die Variablen $\vec x$ meistens implizit angenommen, anstatt sie explizit aufzulisten.

\subsection{Definition des Typsystems und Hilfsmittel}

Das hier vorgestellte Typsystem basiert auf dem Hindley-Milner Typsystem [\cite{MILNER1978348}], welches die Grundlage für die Sprache ML bildet.
Der Formalismus definiert \emph{monomorphe} $\tau$ und \emph{polymorphe} $\sigma$ Typen; erstere sind Typvariablen oder Typapplikationen, letztere sind über eine Typvariable quantisierte Typen [\cite[S.351,355]{MILNER1978348}].
Für diese Implementierung des Typsystems wird für die quantisierten, mit anderen Worten \emph{gebundenen} Typvariablen, eine \emph{lokale namenlose Repräsentierung} in Form der Kodierung durch \emph{De-Bruijn Indizes} verwendet [\cite{DeBruijn1972}].
Diese Repräsentierung vereinfacht den Vergleich und die später definierte \emph{Instanziierung} von polymorphen Typen, indem gebundene Variablen nicht durch Namen, sondern durch die Distanz zu ihren \emph{Binder} beschrieben werden.
In der Abhandlung bezieht sich De-Bruijn auf Lambda-Ausdrücke, aber das Konzept kann auf polymorphe Typen übertragen werden, da diese effektiv Funktionen auf der Typ-Ebene sind.
Die Art des hier verwendeten Polymorphismus ist daher auch \emph{parametrischer Polymorphismus}; Typen sind also, wie Funktionen, parametrisiert, was sich bereits durch die ähnliche Schreibweise zeigt: $\lambda x . \, x \approx \forall \alpha . \, \alpha \to \alpha$.
So wird beispielsweise intern der Typ der Funktion \lstinline|swap| anstatt durch $\forall a \; b . \, a \; b - b \; a$ durch $\forall a \; b . \, 1 \; 0 - 0 \; 1$ dargestellt, da $b$ durch den letzten Binder eingeführt ($\Rightarrow 0$) wurde und ein Binder übersprungen werden muss, um den Binder von $a$ zu erreichen ($\Rightarrow 1$).
Nicht gebundene Variablen werden als \emph{freie} Variablen bezeichnet und durch ihren Namen beschrieben [\cite[S.
383-385]{DeBruijn1972}].
Die Typen des hier vorgestellten Typsystem lassen sich damit wie folgt definieren:

\setlength\arraycolsep{2pt}
\begin{equation}
    \array{llrlp{5mm}l}
    \text{Mono-Typ} & \tau & ::= & \text{Free} \; \alpha && \text{freie Typ-Variable}\\
                    &      &   | & \text{Bound} \; i && \text{gebundene Typ-Variable}\\
                    &      &   | & \text{Meta} \; \alpha? && \text{Meta-Variable}\\
                    &      &   | & ? && \text{unbekannter Typ}\\
                    &      &   | & [ \tau_1, \dots, \tau_n ] && \text{Stack}\\
                    &      &   | & \tau_1 \dots \tau_n - \tau_{n + 1} \dots \tau_{n + m} && \text{Funktion}\\
                    &      &   | & \{ [ \tau_1, \dots, \tau_n ] \; [ \tau_{n + 1}, \dots, \tau_{n + m} ] \} && \text{Map}\\\\
    \text{Poly-Typ} & \sigma & ::= & \text{Mono} \; \tau && \text{Mono-Typ}\\
                    &        &   | & \text{Poly} \; \forall \alpha_1 \dots \alpha_n . \, \tau && \text{Quantisierter Typ}
    \endarray
\end{equation}

Die üblichen Typapplikationen wurden zur Vereinfachung direkt als Stacks, Funktionen oder Maps dargestellt.
Weiterhin ist nur eine Quantisierung auf oberster Ebene erlaubt, Typen der Art $\forall \alpha_1 \dots \alpha_n . \, \sigma$ werden also abgelehnt\footnote{Die Gestattung solcher Typen erlaubt \emph{arbitrary-rank-types}, die außerhalb des Rahmens dieser Ausarbeitung liegen [\cite{rank-n-types}].}.
Der Typ \enquote{$?$} wird benötigt, da durch die beliebigen Meta-Ebenen von Consize das aktuelle Typsystem auf unentscheidbare Probleme stößt und keinen eindeutigen Typen bestimmen kann\footnote{Dies hängt auch direkt mit dem Fehlen der \emph{arbitrary-rank-types} zusammen.}.
Des Weiteren schreiben wir $\tau \stackcons s$, um ein Element $\tau$ auf einen Stack $s$ zu legen, $[ \tau_1, \dots, \tau_n ]$, um die Elemente eines Stacks aufzulisten und $s_1 \stackconcat s_2$, um zwei Stacks zu konkatenieren.
Die Notation $\tau_1 \dots \tau_n$ bezeichnet eine Typliste, die implizit in einen Stack $[ \tau_1, \dots, \tau_n ]$ umgewandelt werden kann und umgekehrt.

Zur Definition der Inferenzregeln für ein Typsystem von Consize benötigen wir \emph{Unifikation}.
Dabei geht es um das Lösen von Gleichungssystemen, konkreter für Consize bedeutet das, das Urteil $\sigma_1 =^? \sigma_2$, also \enquote{ist $\sigma_1$ \emph{unifizierbar} ($\approx$ \enquote{\emph{gleichmachbar}}) mit $\sigma_2$}, zu fällen [\cite[S.326-329]{pierce2002types}].
Ein Ansatz, polymorphe Typen zu unifizieren, ist diese zuerst zu \emph{instanziieren} und dann zu unifizieren.
Dabei wird der polymorphe Typ $\forall \alpha_1 \dots \alpha_n . \, \tau$ in einen monomorphen $\tau'$ umgewandelt und konsistent jede gebundene Variable $\alpha_i$ durch eine Meta-Variable $\alpha_i?$ in $\tau$ ersetzt.
Beispielsweise liefert die Instanziierung von $\forall a \; b . \, 1 \; 0 - 0 \; 1$ den monomorphen Typ $a? \; b? - b? \; a?$.
Diese Meta-Variablen werden bei der Unifikation instanziiert und verhalten sich wie Variablen in Programmiersprachen.
Wenn $a?$ mit $a? := \mathlstinline{Int}$ instanziiert wird, dann wird das vorherige Beispiel zu $(a? := \mathlstinline{Int}) \; b? - b? \; (a? := \mathlstinline{Int})$ bzw. $\mathlstinline{Int} \; b? - b? \; \mathlstinline{Int}$.
Die Instanziierung von polymorphen Typen \emph{ersetzt} also gebundene Variablen durch Meta-Variablen und die Instanziierung von Meta-Variablen \emph{weißt} ihnen einen Wert \emph{zu}.
Die Definition des Unifikationsalgorithmus wird der Intuition des Lesers überlassen, die zugehörige Implementierung ist in der Funktion \lstinline|Typing.unify| zu finden.
Die Notation einer Unifikation ist $\mathit{unify}(\tau_1, \tau_2) \Rightarrow S_{\bot}$, wobei $S_{\bot}$ bedeutet, dass bei erfolgreicher Unifikation die Substitution $S$ das Ergebnis ist oder dass die Unifikation fehlschlagen kann, was mit $\bot$ notiert wird.
Eine fehlgeschlagene Unifikation führt immer zu einem Typfehler.

\subsection{Definition des Inferenz-Algorithmus}

Mithilfe der Unifikation und den Typdefinitionen können nun die Inferenzregeln des Typsystem definiert werden.
Jede Regel hat dabei einen impliziten Kontext $\Gamma = cs, ts, \mathit{dict}$, wobei $cs$ den \emph{Call-Stack}, $ts$ den aus Mono-Typen bestehenden \emph{Type-Stack} und \emph{dict} das Wörterbuch beschreiben.
Das Ergebnis der Inferenzregeln ist immer ein neuer Type-Stack oder eine Fehlermeldung.
Allgemein lässt sich die Typinferenz eines Programms durch eine einfache Regel beschreiben:

\begin{equation}
  \inferencerule{i \stackcons \mathit{rcs} = \mathit{cs} \qquad \mathit{infer}(i) = \mathit{ts}'}{\mathit{ts}'}
\end{equation}

Diese Regel wird so lange angewendet, bis der Call-Stack des Programms leer ist; der resultierende Type-Stack ist der Typ des Programms.
Die Inferenz eines Objekts wird durch die Inferenzregeln der Hilfsfunktion $\mathit{infer}$ beschrieben, die ein Objekt vom Stack entgegennimmt und einen Type-Stack liefert.
Wenn die Länge einer Typ-Liste relevant ist, ist die Länge durch eine tiefgestellte Zahl beziehungsweise eine Variable angegeben.

\noindent
$\boxed{\mathit{infer}(i)}$ mit Item $i$:

\begin{equation}
  \inferencerule{i \in \mathlstinline{Wrd} \qquad i \notin \mathit{dict}}{\mathlstinline{Wrd} \stackcons \mathit{ts}}[unknown word] \qquad \inferencerule{i \in \mathbb{Z}}{\mathlstinline{Int} \stackcons \mathit{ts}}[integer]
\end{equation}

Die einfachste Inferenz ist für Wörter, die nicht im Wörterbuch stehen und Zahlen, die zum Typ \lstinline|Wrd| bzw. \lstinline|Int| inferiert und auf den Type-Stack gelegt werden.

\begin{equation}
  \inferencerule{i \in \mathlstinline{Wrd} \qquad \mathit{dict}(i) = i' \qquad \mathit{infer}(i') = \mathit{ts}'}{\mathit{ts}'}[known word]
\end{equation}

Bei bekannten Wörtern wird der Typ des entsprechenden Eintrags inferiert und das Ergebnis dieser Inferenz ist auch hier das Ergebnis.

\begin{equation}
  \inferencerule{[ a_n ] = i \qquad \textit{infer-quot}(a_n) = \tau}{\tau \stackcons \mathit{ts}}[quotation]\\\\
\end{equation}

\begin{equation}
  \inferencerule{\{ (k \times v)_n \} = i \qquad \tau_k = \textit{infer-list}(k_n) \qquad \tau_v = \textit{infer-list}(v_n)}{\{ [ \tau_k ] \; [ \tau_v ] \} \stackcons \mathit{ts}}[mapping]
\end{equation}

Stacks bzw. Quotierungen und Maps werden mit ihren entsprechenden Hilfsfunktionen inferiert und auf den Type-Stack gelegt.
Maps haben dabei $n$ Paare der Form $\text{Item} \times \text{Item}$.

\begin{equation}
  \inferencerule{i \in \mathlstinline{Fct} \qquad \textit{infer-app}(i) = ts'}{ts'}[application]
\end{equation}

Zuletzt wird die Inferenz von Funktionen auch durch eine Hilfsfunktionen berechnet, die einen neuen Type-Stack als Ergebnis liefert.

\noindent
$\boxed{\textit{infer-app}(\mathit{in}_n - \mathit{out}_m)}$:

\begin{equation}
  \label{eq:application-under-applied}
  \inferencerule{\Gamma \vdash ts_l \qquad n > l}{\text{type error: under-applied function}}[under applied]\\\\
\end{equation}

  %\inferencerule{
  %  \array{c}
  %  \mathit{tos}_n, \mathit{rts}_k = \mathit{ts}_l \qquad \mathit{ret}_m = t_1? \dots t_m?\\
  %  \mathit{unify}(\mathit{in}_n - \mathit{out}_m, \mathit{tos}_n - \mathit{ret}_m) \Rightarrow \bot
  %  \endarray
  %}{\text{type error: unification failure}}[unification failure]\\\\

\begin{equation}
  \label{eq:application-fully-applied}
  \inferencerule{
    \array{c}
    \mathit{tos}_n \stackconcat \mathit{rts}_k = \mathit{ts}_l \qquad \mathit{ret}_m = t_1? \dots t_m?\\
    \mathit{unify}(\mathit{in}_n - \mathit{out}_m, \mathit{tos}_n - \mathit{ret}_m) \Rightarrow S_{\bot}
    \endarray
  }{S(\mathit{ret}_m) \stackconcat \mathit{rts}_k}[fully applied]
\end{equation}

Die Inferenz von Funktionsanwendungen wird in die zwei gelisteten Fälle unterteilt.
Wenn zu wenig Argumente auf dem Type-Stack liegen (\ref{eq:application-under-applied}), gibt es den Typfehler der nicht vollständig angewendeten Funktion.
Wenn genügend Argumente auf dem Type-Stack liegen (\ref{eq:application-fully-applied}), werden $n$ Elemente vom Stack genommen und als \emph{Top-of-Stack} (TOS) bezeichnet, der Rest der Länge $k$ mit $k = l - n$ bleibt unverändert.
Zusätzlich wird ein Ausgabe-Stack $\mathit{ret}_m$ der Länge $m$ mit generierten Meta-Variablen gefüllt und schließlich wird die Eingabefunktion mit der Funktion $\mathit{tos}_n - \mathit{ret}_m$ unifiziert.
Falls die Unifikation fehlschlägt (notiert durch $S_{\bot}$), gibt es einen Typfehler, ansonsten wird die Substitution $S$ auf das Ergebnis $\mathit{ret}_m$ angewendet\footnote{Die Substitution könnte ebenfalls auf $\mathit{out}_m$ angewendet werden, da sie nach Anwendung der Substitution gleich sind.} und wieder mit $\mathit{rts}_m$ konkateniert.

\noindent
$\boxed{\textit{infer-quot}(a_n)}$:

\begin{equation}
  \label{eq:quotation-stack}
  \inferencerule{\textit{infer-quot-step}(a_n, [], 0) = [\tau_1, \dots, \tau_m], 0}{[\tau_1, \dots, \tau_m]}
\end{equation}

\begin{equation}
  \label{eq:quotation-function}
  \inferencerule{\textit{infer-quot-step}(a_n, [], 0) = [\tau_1, \dots, \tau_m], c}{?_1 \dots ?_c - \tau_1 \dots \tau_m}
\end{equation}

Die Inferenz einer Quotierung ist eine Linksfaltung, die mit einem leeren Stack-Effekt und 0 erwarteten Argumenten startet.
Das Ergebnis der \emph{step}-Funktion ist der akkumulierte Stack-Effekt (Type-Stack) und die Anzahl der erwarteten Argumente.
Sind die Argumente $c \geq 0$, so wird eine Funktion zurückgegeben, die $c$ unbekannte Argumente erwartet (\ref{eq:quotation-function}), ansonsten wird der Stack-Effekt zurückgegeben (\ref{eq:quotation-stack}).

\noindent
$\boxed{\textit{infer-quot-step}(i, e_l, c)}$ mit Objekt $i$, akkumuliertem Stack-Effekt $e_l$ und Anzahl der erwarteten Argumente $c$.
Die Rückgabe ist ein Tupel mit einem neuem Stack-Effekt $e'$ und neuer Anzahl erwarteter Argumente $c'$:

\begin{equation}
  \label{eq:quotation-step-fill}
  \inferencerule{
    \array{c}
    \mathit{infer}(i) = \mathit{in}_n - \mathit{out}_m \qquad l < n \qquad \mathit{tos}_n = e_l \stackconcat [?_{l + 1}, \dots, ?_{n}]\\
    \mathit{ret}_m = t_1? \dots t_m? \qquad \mathit{unify}(\mathit{in}_n - \mathit{out}_m, \mathit{tos}_n - \mathit{ret}_m) \Rightarrow S_{\bot}
    \endarray
  }{S(\mathit{ret}_m), c + (n - l)}[under applied]
\end{equation}

\begin{equation}
  \label{eq:quotation-apply}
  \inferencerule{
    \array{c}
    \mathit{infer}(i) = \mathit{in}_n - \mathit{out}_m \qquad l \geq n \qquad \mathit{tos}_n \stackconcat \mathit{re}_k = e_l\\
    \mathit{ret}_m = t_1? \dots t_m? \qquad \mathit{unify}(\mathit{in}_n - \mathit{out}_m, \mathit{tos}_n - \mathit{ret}_m) \Rightarrow S_{\bot}
    \endarray
  }{S(\mathit{ret}_m) \stackconcat \mathit{re}_k, c}[fully applied]
\end{equation}

\begin{equation}
  \label{eq:quotation-push}
  \inferencerule{\mathit{infer}(i) = \tau}{\tau \stackcons e_l, c}[item]
\end{equation}

Die \emph{step}-Funktion füllt den Stack-Effekt $e_l$ mit unbekannten Typen auf, um die Anzahl der erwarteten Argumente zu bieten, wenn die Funktion mehr Objekte erwartet als gerade auf dem Stack-Effekt liegen (\ref{eq:quotation-step-fill}) und erhöht die Anzahl der erwarteten Argumente.
Ansonsten werden wie bei der normalen Anwendung $n$ Elemente vom Stack-Effekt genommen und zur Unifizierung genutzt (\ref{eq:quotation-apply}).
Wenn der Typ des Objekts keine Funktion ist, wird der Typ auf den Stack-Effekt gelegt (\ref{eq:quotation-push}).

\noindent
$\boxed{\textit{infer-list}(a_n)}$ bildet jedes Objekt $i \in a_n$ auf $\mathit{infer}(i)$, Funktionen werden also nicht angewendet.

\subsection{Anmerkungen}

Ein Beweis der Korrektheit (engl. \emph{Soundness}) bzw. die Typsicherheit des Inferenz-Algorithmus liegt außerhalb des Rahmens dieser Ausarbeitung.
Die entsprechende Beweisstrategie sieht vor, die Sätze \emph{Preservation} und \emph{Progress} für diesen Formalismus zu beweisen.
Ersteres beweist den Erhalt der Typen bei der Evaluation, letzteres dass Ausdrücke entweder vollständig evaluiert sind oder noch weiter evaluiert werden können [\cite[S.55-57]{Harper_2016}].
Des Weiteren können die Inferenzregeln möglicherweise vereinfacht oder zusammengelegt werden, beispielsweise unterscheiden sich die Regeln $\textit{infer-app}$ und $\textit{infer-quot}$ nur darin, dass letztere den Stack mit unbekannten erwarteten Parametern befüllt, während erstere bei zu wenig Argumenten einen Typfehler produziert.
Außerdem ist die Behandlung von Stacks und Maps nicht so mächtig wie möglich, da keine \emph{arbitrary-rank-types} und auch nicht die Anzahl oder die Struktur der Elemente auf den Stacks und Maps behandelt werden.
Der hier vorgestellte Algorithmus bietet also eine Basis, die mit mächtigeren Mitteln erweitert werden kann, um eine ausdrucksstärkere Typisierung von Programmen zu ermöglichen.

Abhilfe schafft hierbei die Funktion $\mathit{check}$, die einen Typ $\sigma$ sowie eine Quotierung $i$ erwartet und prüft, ob $\mathit{infer}(i) =^? \sigma$.
Wenn sie unifizierbar sind, wäre es möglich, den potentiell quantisierten Typ $\sigma$ anstatt den weniger aussagekräftigen Mono-Typ von $\mathit{infer}(i)$ als Typ der Quotierung zu verwenden.

\section{Implementierung}

In diesem Kapitel wird ein kurzer Blick auf die Implementierung der Datenstrukturen und des Algorithmus geworfen, sowie auf die Integration in den bisherigen Consize-Interpreter.

Die Implementierung der Datenstrukturen für die Typen befindet sich im Module \lstinline|Cs_type| (Consize Type).
Die Datenstruktur für die Typen sieht beispielsweise wie folgt aus\footnote{Die Syntax \lstinline[basicstyle=\ttfamily\footnotesize]|a * b| kodiert in OCaml ein Tupel (Produkt-Typ) und \lstinline[basicstyle=\ttfamily\footnotesize]{a | b} eine Type-Union (Summen-Typ).}:

\codebeginbracket
\begin{lstlisting}[language=OCaml]
type mono =
  | Unknown
  | Free of string
  | Bound of int
  | Stack of mono list
  | Map of mono list * mono list
  | Fun of mono list * mono list (* newly introduced types *)
  | Meta of meta_var

type t =
  | Mono of mono
  | Poly of string list * mono
\end{lstlisting}
\codeendbracket

Das Modul zur Integration in den Consize-Interpreter ist \lstinline|Typing| (Pfad: \lstinline|typing/typing.mli|) und hat die folgende Signatur (die entsprechenden Funktionen werden exportiert).
Jede exportierte Funktion repräsentiert eine neue Funktion des Kernels; die Implementierung des Typsystems ist damit eine Erweiterung des Kernels:

\codebeginbracket
\begin{lstlisting}[language=OCaml]
(** typing.check ( ctx type quot -- t/error *)
val check : stack -> stack option

(** typing.infer ( ctx quot -- type ) *)
val infer : stack -> stack option

(** typing.read-type ( wrd -- type ) *)
val read_type : stack -> stack option

(** typing.show-type ( ctx type -- wrd ) *)
val show_type : stack -> stack option
\end{lstlisting}
\codeendbracket

Die Wörter, wie sie in Consize verwendet werden können, stehen als Dokumentations-Kommentar über der Funktion.
Damit ist es zum Beispiel möglich, Consize das folgende Programm ausführen zu lassen.

\begin{lstlisting}
[ [ 1 ] 1 dup rot swap ] typing.infer typing.show-types
\end{lstlisting}

Die Funktion \lstinline|typing.show-types| ist dabei in der mitgelieferten Bibliothek \lstinline|typing.txt| definiert, mit der Typen textuell dargestellt werden können.
Die Ausgabe dieses Programms wäre damit:

\begin{lstlisting}
Int [ Int ] Int
\end{lstlisting}

Die ebenfalls beigefügten Tests in \lstinline|typing-tests.txt| prüfen die Funktionen, zum Beispiel auch das Lesen und Ausgeben von Typen:

\begin{lstlisting}
% signature of push
( ( forall A B . [ A ] repr B -- [ A B ] repr ) repr )
  [ [ forall A B . [ A ] B -- [ A B ] ] typing.read-type typing.show-type ]
  unit-test
\end{lstlisting}

Die interne Kodierung von Funktionen kann schnell zu Problemen führen, deren Schwere und Lösung abhängig davon sind, ob das oberste Element links oder rechts geschrieben wird.
Nach einigen Ansätzen hat es sich als praktisch erwiesen, bei der textuellen Repräsentation das oberste Element \emph{rechts} (\emph{rechts ausgerichtet}) und intern \emph{links} (\emph{links ausgerichtet}) zu verwenden.
Dieser Ansatz ist nah an der Darstellung der Stacks, die ohne Verschachtelung auch rechts, aber ansonsten links ausgerichtet sind.
Der Grund für die links ausgerichtete interne Kodierung ist, dass alle Stacks intern als verkettete Listen repräsentiert werden und der Top-of-Stack demnach ebenfalls links ausgerichtet ist.
Damit wird das Entnehmen oder Hinzufügen vereinfacht, da alle Listen automatisch korrekt geordnet sind.
Für das Einlesen und die Ausgabe werden die Listen demnach umgedreht.

Die Integration von Typen hat sich auch auf die Definition von Werten (Objekten) ausgewirkt, die um ein Typ-Objekt erweitert wurden.
Ebenso führen Funktionsobjekte nun ihren Typen mit sich, um eine Inferenz zu ermöglichen.

\codebeginbracket

\begin{lstlisting}[language=OCaml]
type item =
  | Word of string
  | Stack of stack
  | Map of dictionary
  | Fun of (stack -> stack option) * Cs_type.t
  | Type of Cs_type.t
  | Nil
\end{lstlisting}

\codeendbracket

Die Einträge des initialen Wörterbuchs wurden um ihre Typsignaturen erweitert.

\codebeginbracket
\begin{lstlisting}[language=OCaml]
let entries =
  [ Word "swap", Fun (Shuffler.swap, Shuffler.swap_type); ... ]

let swap_type : Cs_type.t =
  Poly ([ "a"; "b" ], Fun ([ Bound 1; Bound 0 ], [ Bound 0; Bound 1 ]))
\end{lstlisting}
\codeendbracket

Eine wichtige Definition aus dem Programm \lstinline|typing.txt| ist die \lstinline|::|, die anders als \lstinline|:| nicht nur einen Stack-Effekt entgegennimmt, der verworfen wird, sondern eine Typsignatur, die zur Überprüfung der Definition verwendet wird.

\codebeginbracket
\begin{lstlisting}
: :: ( name type-quot impl -- )
  [ ( ) swap scan4; destruct-definition unpush typing.read-types
    swap dup rot typing.check
    dup t equal? [ drop def+ ] [ -rot4 3drop ] if continue
  ] call/cc ;

:: five [ Int ] 5 ;
:: inc [ [ Int -- Int ] ] 1 + ;
:: six [ Int ] five inc ;
\end{lstlisting}
\codeendbracket

Die Unifikation wurde mit Hilfe von Meta-Variablen umgesetzt, die bei der Instanziierung eines polymorphen Typen erzeugt werden.

\codebeginbracket
\begin{lstlisting}[language=OCaml]
type meta_var =
  { mutable instance : meta_var_instance
  ; name : string
  ; id : int
  }
\end{lstlisting}
\codeendbracket

Diese Meta-Variablen werden bei der Unifikation instanziiert, wenn sie zum Beispiel mit einer freien Variable unifiziert werden sollen: $a? =^? \text{free}(\text{Int}) \implies a? := \text{free}(\text{Int})$.
Sowohl für die Unifikation als auch für die restliche Inferenz können Debug-Ausgaben aktiviert werden, indem \lstinline|show_unify_logs| bzw. \lstinline|show_inference_logs| den Wert \lstinline|true| erhalten.
Die Debug-Ausgaben sehen beispielsweise wie folgt aus:

\begin{lstlisting}
unify '[ ? -- t0? t1? ]' with '[ a? -- a? a? ]'
==> unify '[ ? -- t0? t1? ]' with '[ a? -- a? a? ]'
    ==> unify '?' with 'a?'
    ==> ? := a?   (t, mv)
    ==> unify 'a?' with '?'
    <== a? := ?   (mv, t)
    ==> unify 't1?' with '(a? := ?)'
    <== t1? := (a? := ?)   (mv, mv)
    ==> unify 't0?' with '(a? := ?)'
    <== t0? := (a? := ?)   (mv, mv)
<== unified '[ ? -- (t0? := (a? := ?)) (t1? := (a? := ?)) ]'
       with '[ (a? := ?) -- (a? := ?) (a? := ?) ]'
\end{lstlisting}

\section{Fazit und Ausblick}

Das implementierte Typsystem bietet eine Grundlage für eine sinnvolle Typisierung von Consize, doch ist es noch nicht mächtig genug.
Es würde für einfache funktionale Sprachen ausreichen, da es auf dem Typsystem von ML basiert, aber damit Consize sinnvoll und weitgehend typisiert werden kann, muss es arbitrary-rank-types und eine Möglichkeit, über die Strukturen von Stacks zu urteilen, geben.

Ohne arbitrary-rank-types ist der inferrierte Typ von $[ \text{swap} \; \text{dup} ]$ immer $[ ? \; ? \; - \; ? \; ? \; ? ]$ und der Typ von $1 \; \text{a} \; [ \text{swap} \; \text{dup} ] \; \text{call}$ nicht wie erhofft $\mathlstinline{Wrd} \; \mathlstinline{Int} \; \mathlstinline{Int}$, sondern nur $? \; ? \; ?$.
Für den genaueren Typ, müssen die Quotierungen ihren polymorphe Signatur behalten und zu $[ \forall a \; b . \; a \; b - b \; a \; a ]$ inferiert werden.

Die Beschreibung von Stacks und Maps durch Typsignaturen stellt sich auch nicht als leichtes Problem heraus.
Erstens ist die Länge der Stacks eine Zahl, die abhängig von der Benutzereingabe sein kann.
Das führt in das Thema von \emph{dependent types}, bei dem Typen von Werten abhängen dürfen und in dem es auch eine Liste mit einer Länge gibt: $\text{Vec} \; A \; n$ wobei $A$ ein Typ und $n$ eine natürliche Zahl ist.
Eine Notation für Consize könnte $[ a_n ]$ sein, wobei $a$ ein Typ und $n$ die Länge ist.
Die Struktur der Stacks müsste mit Folgen (Produkt-Typen) oder Alternativen (Summen-Typen) beschrieben werden.
Der Stack $[ 1 \; \text{a} \; 2 \; \text{b} ]$ könnte zum Beispiel den Typ $[ (\mathlstinline{Int} \; \mathlstinline{Wrd})_2 ]$ haben.
Signaturen wie die von \lstinline|mapping| lassen sich so bereits beschreiben: $\mathlstinline{mapping} \; [ \forall a \; b . \; [ (a \; b)_n ] - \{ [a_n] \; [b_n] \}]$.
Diese Darstellung kommt bei der Funktion \lstinline|dissoc| aber bereits an seine Grenzen: $\mathlstinline{dissoc} \; [ \forall a \; b . \; \{ [a_n] \; [b_n] \} \; a - \{ [a_{(n \; | \; n - 1)}] \; [b_{(n \; | \; n - 1)}] \} ]$, da entweder ein Wert entfernt wurde, was die Anzahl der Schlüssel und Werte um jeweils eins verringert, oder nicht.
Es wäre aber denkbar, die Maps auf mehrere Arten darstellen zu können, beispielsweise könnte \lstinline|dissoc| dann wie folgt definiert werden:

\[
  \mathlstinline{dissoc} \; [ \forall a \; b \; c \; d \; e \; f . \; \{ (a \; b)_n \; (c \; d) \; (e \; f)_m \} \; c - \{ (a \; b)_n \; (e \; f)_m \} ].
\]

Diese Kodierung würde aber dafür sorgen, dass \lstinline|dissoc| nur auf Maps funktioniert, die auch den entsprechenden Schlüssel haben, und somit das Verhalten der ursprünglichen Funktion ändern.
Es zeigt sich, dass für die übrigen Probleme weitere Recherche und Theorien nötig sind, und damit außerhalb des Rahmens dieser Ausarbeitung liegt.
Mit guten Lösungen würde es mit dem Typsystem aber eine sinnvolle Erweiterung für Consize geben, mit der Programme beweisenermaßen sicher und robust werden.

\printbibliography

\end{document}
