(* Note on the code base: Don't be scared of some long functions, they often have locally
 * defined helper functions and are mostly written in a functional paradigm.
 * Exhaustive pattern matches on types encourage longer functions as well as the formatting
 * guidelines where lines longer than 90 characters are split up.
 * The additional surrounding context of long functions can be overwhelming but on the
 * other hand provide useful information to understand the code.
 * On several occasions, the code is bloated up with logging statements which are behind
 * an if condition. These can be mostly ignored or read for further information. *)

open Common
open Cs_type
open Item
open Builtin

let builtins : dictionary =
  let table = Hashtbl.create 100 in
  let entries =
    List.to_seq
      [ Word "swap", Fun (Shuffler.swap, Shuffler.swap_type)
      ; Word "dup", Fun (Shuffler.dup, Shuffler.dup_type)
      ; Word "drop", Fun (Shuffler.drop, Shuffler.drop_type)
      ; Word "rot", Fun (Shuffler.rot, Shuffler.rot_type)
      ; Word "type", Fun (Types.get_type, Types.get_type_type)
      ; Word "equal?", Fun (Types.equal, Types.equal_type)
      ; Word "identical?", Fun (Types.identical, Types.identical_type)
      ; Word "typing.check", Stack [ Fun (Typing.check, Mono Unknown) ]
      ; Word "typing.infer", Stack [ Fun (Typing.infer, Mono Unknown) ]
      ; Word "typing.read-type", Fun (Typing.read_type, Mono Unknown)
      ; Word "typing.show-type", Fun (Typing.show_type, Mono Unknown)
      ; Word "typing.type-info", Fun (Typing.type_info, Mono Unknown)
      ; Word "typing.source", Stack [ Fun (Typing.source, Mono Unknown) ]
      ; Word "emptystack", Fun (Stack_ops.empty_stack, Stack_ops.empty_stack_type)
      ; Word "push", Fun (Stack_ops.push, Stack_ops.push_type)
      ; Word "top", Fun (Stack_ops.top, Stack_ops.top_type)
      ; Word "pop", Fun (Stack_ops.pop, Stack_ops.pop_type)
      ; Word "concat", Fun (Stack_ops.concat, Stack_ops.concat_type)
      ; Word "reverse", Fun (Stack_ops.reverse, Stack_ops.reverse_type)
      ; Word "mapping", Fun (Map_ops.mapping, Map_ops.mapping_type)
      ; Word "unmap", Fun (Map_ops.unmap, Map_ops.unmap_type)
      ; Word "assoc", Fun (Map_ops.assoc, Map_ops.assoc_type)
      ; Word "dissoc", Fun (Map_ops.dissoc, Map_ops.dissoc_type)
      ; Word "get", Fun (Map_ops.get, Map_ops.get_type)
      ; Word "keys", Fun (Map_ops.keys, Map_ops.keys_type)
      ; Word "merge", Fun (Map_ops.merge, Map_ops.merge_type)
      ; Word "word", Fun (Word_ops.word, Word_ops.word_type)
      ; Word "unword", Fun (Word_ops.unword, Word_ops.unword_type)
      ; Word "char", Fun (Word_ops.char, Word_ops.char_type)
      ; Word "print", Fun (Io.print, Io.print_type)
      ; Word "flush", Fun (Io.flush, Io.flush_type)
      ; Word "read-line", Fun (Io.read_line, Io.read_line_type)
      ; Word "slurp", Fun (Files.slurp, Files.slurp_type)
      ; Word "spit", Fun (Files.spit, Files.spit_type)
      ; Word "spit-on", Fun (Files.spit_on, Files.spit_on_type)
      ; Word "uncomment", Fun (Text.uncomment, Text.uncomment_type)
      ; Word "tokenize", Fun (Text.tokenize, Text.tokenize_type)
      ; Word "undocument", Fun (Text.undocument, Text.undocument_type)
      ; ( Word "current-time-millis"
        , Fun (Os.current_time_millis, Os.current_time_millis_type) )
      ; Word "operating-system", Fun (Os.operating_system, Os.operating_system_type)
      ; Word "call", Stack [ Fun (Meta.call, Mono Unknown) ]
      ; Word "call/cc", Stack [ Fun (Meta.callcc, Mono Unknown) ]
      ; Word "continue", Stack [ Fun (Meta.continue, Mono Unknown) ]
      ; Word "get-dict", Stack [ Fun (Meta.get_dict, Mono Unknown) ]
      ; Word "set-dict", Stack [ Fun (Meta.set_dict, Mono Unknown) ]
      ; Word "apply", Fun (Functions.apply, Functions.apply_type)
      ; Word "compose", Fun (Functions.compose, Functions.compose_type)
      ; Word "func", Fun (Functions.func, Functions.func_type)
      ; Word "integer?", Fun (Arithmetics.is_integer, Arithmetics.is_integer_type)
        (* Arithmetics *)
      ; Word "+", Fun (Arithmetics.binary Arithmetics.Add, Arithmetics.arithmetic_type)
      ; Word "-", Fun (Arithmetics.binary Arithmetics.Sub, Arithmetics.arithmetic_type)
      ; Word "*", Fun (Arithmetics.binary Arithmetics.Mul, Arithmetics.arithmetic_type)
      ; Word "div", Fun (Arithmetics.binary Arithmetics.Div, Arithmetics.arithmetic_type)
      ; Word "mod", Fun (Arithmetics.binary Arithmetics.Mod, Arithmetics.arithmetic_type)
      ; Word "<", Fun (Arithmetics.binary Arithmetics.Lt, Arithmetics.comparison_type)
      ; Word ">", Fun (Arithmetics.binary Arithmetics.Gt, Arithmetics.comparison_type)
      ; Word "==", Fun (Arithmetics.binary Arithmetics.Eq, Arithmetics.comparison_type)
      ; Word "<=", Fun (Arithmetics.binary Arithmetics.Le, Arithmetics.comparison_type)
      ; Word ">=", Fun (Arithmetics.binary Arithmetics.Ge, Arithmetics.comparison_type)
      ; Word "stepcc", Fun (Functions.stepcc, Mono Unknown)
      ; ( Word "\\"
        , Stack
            [ Stack
                [ Word "dup"
                ; Word "top"
                ; Word "rot"
                ; Word "swap"
                ; Word "push"
                ; Word "swap"
                ; Word "pop"
                ; Word "continue"
                ]
            ; Word "call/cc"
            ] )
      ; Word "load", Stack [ Word "slurp"; Word "uncomment"; Word "tokenize" ]
      ; Word "run", Stack [ Word "load"; Word "call" ]
      ; ( Word "start"
        , Stack
            [ Word "slurp"
            ; Word "uncomment"
            ; Word "tokenize"
            ; Word "get-dict"
            ; Word "func"
            ; Word "emptystack"
            ; Word "swap"
            ; Word "apply"
            ] )
      ]
  in
  Hashtbl.add_seq table entries;
  table
;;

exception Interpreter_error of string

let main () : unit =
  let tokens : stack option =
    let aux acc arg = acc ^ " " ^ arg in
    [ Word (List.fold_left aux "" (List.tl (Array.to_list Sys.argv))) ]
    |> Builtin.Text.uncomment
    |> Option.map Builtin.Text.tokenize
    |> Option.join
  in
  match tokens with
  | Some [ Stack program ] ->
    (match Builtin.Functions.func [ Map builtins; Stack program ] with
     | Some [ Fun (f, _) ] ->
       (match Builtin.Functions.apply [ Fun (f, Mono Unknown); Stack [] ] with
        | Some (Stack result :: _) ->
          Printf.printf
            "Consize returns (%s)\n"
            (List.fold_left (fun a v -> a ^ " " ^ show_item v) "" result)
        | _ -> raise (Interpreter_error "(Lv. 3) applying the initial function failed"))
     | _ -> raise (Interpreter_error "(Lv. 2) constructing the initial function failed"))
  | _ -> raise (Interpreter_error "(Lv. 1) program could not be tokenized")
;;

let () = main ()
