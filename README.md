# Consize OCaml

A [Consize](https://github.com/denkspuren/consize) interpreter implemented in [OCaml](https://ocaml.org/).

## Requirements

- Option 1: You can use [Docker](https://www.docker.com/) to run the application within a container.
- Option 2: You can install the OCaml package manager [opam](https://opam.ocaml.org/) and then follow the steps in the `Dockerfile`.

## How to build and run

To run the application with Docker you need to build the container with the following command:

```sh
$ docker build -t local/ocaml .
```

The image `local/ocaml` can be executed with the following command:

```sh
$ docker run -it local/ocaml
```

You can build the program in the containerized shell with the following command:

```sh
$ eval $(opam env --safe) # Do this if dune is not found.
$ dune build
```

To run the repl or the tests, use the following commands:

```sh
# repl
$ dune exec _build/default/consize.exe "\ prelude.txt run say-hi"

# tests
$ dune exec _build/default/consize.exe "\ prelude.txt run \ prelude-test.txt run"
$ dune exec _build/default/consize.exe "\ prelude.txt run \ typing.txt run \ typing-tests.txt run"
```

The typing module can be used after the `prelude.txt` and `typing.txt` files are loaded.
To infer the type of quotation (or program) such as `[ 1 a dup rot ]`, use the following command:

```sh
$ dune exec _build/default/consize.exe "\ prelude.txt run \ typing.txt run [ 1 a dup rot rot ] typing.infer typing.show-types"
```

which should produce the following output

```
Consize returns ( [ Int Wrd Wrd ])
```
