open Common
open Item

(** uncomment ( wrd -- wrd' ) *)
val uncomment : stack -> stack option

val uncomment_type : Cs_type.t

(** tokenize ( wrd -- seq ) *)
val tokenize : stack -> stack option

val tokenize_type : Cs_type.t

(** undocument ( wrd -- wrd' ) *)
val undocument : stack -> stack option

val undocument_type : Cs_type.t
