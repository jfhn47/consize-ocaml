open Common
open Item

let empty_stack (ds : stack) : stack option = Some (Stack [] :: ds)
let empty_stack_type : Cs_type.t = Mono (Fun ([], [ Free "Any" ]))

let push : stack -> stack option = function
  | item :: Stack s :: rest -> Some (Stack (item :: s) :: rest)
  | _ -> None
;;

(* ∀ a b . [ a ] b -- [ b a ] *)
let push_type : Cs_type.t =
  Poly
    ( [ "a"; "b" ]
    , Fun ([ Bound 0; Cs_type.Stack [ Bound 1 ] ], [ Cs_type.Stack [ Bound 1; Bound 0 ] ])
    )
;;

let top : stack -> stack option = function
  | Stack (item :: _) :: rest -> Some (item :: rest)
  | Stack [] :: rest -> Some (Nil :: rest)
  | Nil :: rest -> Some (Nil :: rest)
  | ds ->
    Stack ds |> show_item |> Printf.printf "Top did not match! Data stack = %s\n";
    None
;;

let top_type : Cs_type.t = Poly ([ "a" ], Fun ([ Cs_type.Stack [ Bound 0 ] ], [ Bound 0 ]))

let pop : stack -> stack option = function
  | Nil :: rest -> Some (Stack [] :: rest)
  | Stack [] :: rest -> Some (Stack [] :: rest)
  | Stack (_ :: rest2) :: rest -> Some (Stack rest2 :: rest)
  | ds ->
    show_item (Stack ds) |> Printf.printf "pop failed with with data stack = %s\n";
    None
;;

let pop_type : Cs_type.t =
  Poly ([ "a" ], Fun ([ Cs_type.Stack [ Bound 0 ] ], [ Cs_type.Stack [ Bound 0 ] ]))
;;

let reverse : stack -> stack option = function
  | Stack s :: rest -> Some (Stack (List.rev s) :: rest)
  | _ -> None
;;

let reverse_type : Cs_type.t =
  Poly ([ "a" ], Fun ([ Cs_type.Stack [ Bound 0 ] ], [ Cs_type.Stack [ Bound 0 ] ]))
;;

let concat : stack -> stack option = function
  | Stack lower :: Stack upper :: rest -> Some (Stack (upper @ lower) :: rest)
  | _ -> None
;;

let concat_type : Cs_type.t =
  Poly
    ( [ "a"; "b" ]
    , Fun
        ( [ Cs_type.Stack [ Bound 1 ]; Cs_type.Stack [ Bound 0 ] ]
        , [ Cs_type.Stack [ Bound 1; Bound 0 ] ] ) )
;;
