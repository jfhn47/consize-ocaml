open Common
open Item
open Util

let is_integer : stack -> stack option = function
  | Word w :: rest ->
    let result = if String2.is_integer w then "t" else "f" in
    Some (Word result :: rest)
  | _ :: rest -> Some (Word "f" :: rest)
  | _ -> None
;;

let is_integer_type : Cs_type.t = Poly ([ "a" ], Fun ([ Bound 0 ], [ Free "Bool" ]))

type binary_operator =
  | Add
  | Sub
  | Mul
  | Div
  | Mod
  | Lt
  | Gt
  | Eq
  | Le
  | Ge

let binary (operator : binary_operator) : stack -> stack option = function
  | Word rhs :: Word lhs :: rest ->
    let op : int -> int -> item =
      match operator with
      | Add -> fun x y -> Word (x + y |> string_of_int)
      | Sub -> fun x y -> Word (x - y |> string_of_int)
      | Mul -> fun x y -> Word (x * y |> string_of_int)
      | Div -> fun x y -> Word (x / y |> string_of_int)
      | Mod -> fun x y -> Word (x mod y |> string_of_int)
      | Lt -> fun x y -> if x < y then Word "t" else Word "f"
      | Gt -> fun x y -> if x > y then Word "t" else Word "f"
      | Eq -> fun x y -> if x = y then Word "t" else Word "f"
      | Le -> fun x y -> if x <= y then Word "t" else Word "f"
      | Ge -> fun x y -> if x >= y then Word "t" else Word "f"
    in
    (try
       let n1, n2 = (lhs, rhs) |> Tuple.map int_of_string in
       Some (op n1 n2 :: rest)
     with
     | Failure _ -> None)
  | _ -> None
;;

let arithmetic_type : Cs_type.t = Mono (Fun ([ Free "Int"; Free "Int" ], [ Free "Int" ]))
let comparison_type : Cs_type.t = Mono (Fun ([ Free "Int"; Free "Int" ], [ Free "Bool" ]))
