open Common
open Item

let print : stack -> stack option = function
  | Word w :: rest ->
    print_string w;
    Some rest
  | _ -> None
;;

let print_type : Cs_type.t = Mono (Fun ([ Free "Wrd" ], []))

let flush (ds : stack) : stack option =
  flush stdout;
  Some ds
;;

let flush_type : Cs_type.t = Mono (Fun ([], []))

let read_line (ds : stack) : stack option =
  In_channel.input_line stdin |> Option.map (fun w -> Word w :: ds)
;;

let read_line_type : Cs_type.t = Mono (Fun ([], [ Free "Wrd" ]))
