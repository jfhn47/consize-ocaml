open Common
open Item

(** type ( itm -- wrd ) *)
val get_type : stack -> stack option

val get_type_type : Cs_type.t

(** equal ( x y -- t/f ) *)
val equal : stack -> stack option

val equal_type : Cs_type.t

(** identical ( x y -- t/f ) *)
val identical : stack -> stack option

val identical_type : Cs_type.t
