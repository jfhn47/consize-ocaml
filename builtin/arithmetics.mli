open Common
open Item

(** integer? ( x -- t/f ) *)
val is_integer : stack -> stack option

val is_integer_type : Cs_type.t

type binary_operator =
  | Add
  | Sub
  | Mul
  | Div
  | Mod
  | Lt
  | Gt
  | Eq
  | Le
  | Ge

(** binop ( wrd wrd -- wrd ) *)
val binary : binary_operator -> stack -> stack option

val arithmetic_type : Cs_type.t
val comparison_type : Cs_type.t
