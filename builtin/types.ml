open Common
open Item
open Util

let get_type : stack -> stack option = function
  | Word _ :: rest -> Some (Word "wrd" :: rest)
  | Stack _ :: rest -> Some (Word "stk" :: rest)
  | Map _ :: rest -> Some (Word "map" :: rest)
  | Fun _ :: rest -> Some (Word "fct" :: rest)
  | Type _ :: rest -> Some (Word "typ" :: rest)
  | Nil :: rest -> Some (Word "nil" :: rest)
  (* Some (Word ("_|_") :: rest) is not possible because the match is already exhaustive *)
  | [] -> None
;;

let get_type_type : Cs_type.t = Poly ([ "a" ], Fun ([ Bound 0 ], [ Free "Type" ]))

let equal : stack -> stack option = function
  | x :: y :: rest ->
    let rec compare_items (x : item) (y : item) : bool =
      match x, y with
      | Word x, Word y -> x = y
      | Stack xs, Stack ys ->
        (try List.for_all2 compare_items xs ys with
         | _ -> false)
      | Map xs, Map ys ->
        let check_pair left_entry right_entry =
          match left_entry, right_entry with
          | (lk, lv), (rk, rv) -> compare_items lk rk && compare_items lv rv
        in
        (Hashtbl.to_seq xs, Hashtbl.to_seq ys) |> Tuple.uncurry (Seq.for_all2 check_pair)
      | Fun _, Fun _ -> false
      | Nil, Nil -> true
      | _ -> false
    in
    let result = if compare_items x y then Word "t" else Word "f" in
    Some (result :: rest)
  | _ -> None
;;

let equal_type : Cs_type.t =
  Poly ([ "a"; "b" ], Fun ([ Bound 1; Bound 0 ], [ Free "Bool" ]))
;;

let identical : stack -> stack option = equal

let identical_type : Cs_type.t =
  Poly ([ "a"; "b" ], Fun ([ Bound 1; Bound 0 ], [ Free "Bool" ]))
;;
