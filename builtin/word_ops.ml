open Common
open Item
open Util

let word : stack -> stack option = function
  | Stack s :: rest ->
    let is_word = function
      | Word _ -> true
      | _ -> false
    in
    let f acc item = acc ^ show_item item in
    if List.for_all is_word s then Some (Word (List.fold_left f "" s) :: rest) else None
  | _ -> None
;;

let word_type : Cs_type.t = Mono (Fun ([ Stack [ Free "Wrd" ] ], [ Free "Wrd" ]))

let unword : stack -> stack option = function
  | Word w :: rest ->
    let item =
      w
      |> String2.explode
      |> List.map (compose (fun s -> Word s) Char2.to_string)
      |> fun ws -> Stack ws
    in
    Some (item :: rest)
  | _ -> None
;;

let unword_type : Cs_type.t = Mono (Fun ([ Free "Wrd" ], [ Stack [ Free "Wrd" ] ]))
let unicode (code : int) : string = code |> Uchar.of_int |> Uchar.to_char |> String.make 1

let char : stack -> stack option = function
  | Word "\\space" :: rest -> Some (Word " " :: rest)
  | Word "\\newline" :: rest -> Some (Word "\n" :: rest)
  | Word "\\formfeed" :: rest -> Some (Word (unicode 12) :: rest)
  | Word "\\return" :: rest -> Some (Word "\r" :: rest)
  | Word "\\backspace" :: rest -> Some (Word (unicode 8) :: rest)
  | Word "\\tab" :: rest -> Some (Word "\t" :: rest)
  | Word w :: rest ->
    (* This is somehow very cumbersome and the control flow is not intuitive. *)
    let chars = String2.explode w in
    (match chars with
     | [ '\\'; 'u'; _; _; _; _ ] ->
       let pow base exp =
         if exp = 0 then 1 else Seq.repeat base |> Seq.take exp |> Seq.fold_left ( * ) 1
       in
       let to_hex : char -> int option = function
         | c when c >= '0' && c <= '9' -> Some (int_of_char c - 48)
         | c when c >= 'A' && c <= 'F' -> Some (int_of_char c - 65 + 10)
         | c when c >= 'a' && c <= 'f' -> Some (int_of_char c - 97 + 10)
         | _ -> None
       in
       let f sum = function
         | exp, n -> sum + (n * pow 16 exp)
       in
       let aux ints =
         ints
         |> List.to_seq
         |> Seq.zip (List.to_seq [ 3; 2; 1; 0 ])
         |> Seq.fold_left f 0
         |> unicode
       in
       let uni : string option =
         chars |> List2.drop 2 |> List.map to_hex |> Option2.sequence |> Option.map aux
       in
       (match uni with
        | Some uni -> Some (Word uni :: rest)
        | None -> None)
     | _ -> None)
  | _ -> None
;;

let char_type : Cs_type.t = Mono (Fun ([ Free "Wrd" ], [ Free "Wrd" ]))
