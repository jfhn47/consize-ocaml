open Common
open Item

(** print ( wrd -- ) *)
val print : stack -> stack option

val print_type : Cs_type.t

(** flush ( -- ) *)
val flush : stack -> stack option

val flush_type : Cs_type.t

(** read-line ( -- wrd ) *)
val read_line : stack -> stack option

val read_line_type : Cs_type.t
