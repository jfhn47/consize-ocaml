open Common
open Item

(** emptystack ( -- [ ] ) *)
val empty_stack : stack -> stack option

val empty_stack_type : Cs_type.t

(** push ( stk itm -- [ itm & stk ] ) *)
val push : stack -> stack option

val push_type : Cs_type.t

(** top ( [ itm & stk ] -- itm *)
val top : stack -> stack option

val top_type : Cs_type.t

(** pop ( [ itm & stk ] -- stk *)
val pop : stack -> stack option

val pop_type : Cs_type.t

(** reverse ( stk -- stk' )
    * example: emptystack a push b push c push reverse -> [ a b c ] *)
val reverse : stack -> stack option

val reverse_type : Cs_type.t

(** concat ( stk1 stk2 -- stk3 )
    * example: emptystack a push dup concat -> [ a a ] *)
val concat : stack -> stack option

val concat_type : Cs_type.t
