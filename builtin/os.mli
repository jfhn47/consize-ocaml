open Common
open Item

(** current-time-millis ( -- wrd ) *)
val current_time_millis : stack -> stack option

val current_time_millis_type : Cs_type.t

(** operating-system ( -- wrd ) *)
val operating_system : stack -> stack option

val operating_system_type : Cs_type.t
