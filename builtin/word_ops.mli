open Common
open Item

(** word ( seq -- wrd ) *)
val word : stack -> stack option

val word_type : Cs_type.t

(** unword ( wrd -- seq ) *)
val unword : stack -> stack option

val unword_type : Cs_type.t

(* Note: The unicode char code is computated correctly but
 * they cannot be printed because the unicode function converts
 * the unicode back to an ASCII string. Unicode handling does
 * not seem to be easy in OCaml -- se the branch "unicode" where
 * the code gets more bloated and does not even work at the moment. *)

(** char ( wrd -- wrd' ) *)
val char : stack -> stack option

val char_type : Cs_type.t
