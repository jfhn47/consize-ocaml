open Common
open Item

let current_time_millis (ds : stack) : stack option =
  let time =
    Unix.gettimeofday () |> Float.mul (float_of_int 1000) |> int_of_float |> string_of_int
  in
  Some (Word time :: ds)
;;

let current_time_millis_type : Cs_type.t = Mono (Fun ([], [ Free "Wrd" ]))
let operating_system (ds : stack) : stack option = Some (Word Sys.os_type :: ds)
let operating_system_type : Cs_type.t = Mono (Fun ([], [ Free "Wrd" ]))
