open Common
open Item

(** swap ( x y -- y x ) *)
val swap : stack -> stack option

val swap_type : Cs_type.t

(** dup ( x -- x x ) *)
val dup : stack -> stack option

val dup_type : Cs_type.t

(** drop ( x -- ) *)
val drop : stack -> stack option

val drop_type : Cs_type.t

(** rot ( x y z -- y z x ) *)
val rot : stack -> stack option

val rot_type : Cs_type.t
