open Common
open Item

let uncomment_re = Str.regexp "[ \t\n\r]*%.*\r?\n"

let uncomment : stack -> stack option = function
  | Word w :: rest ->
    let f acc s = acc ^ "\n" ^ s in
    let code = Str.split uncomment_re w |> List.fold_left f "" in
    Some (Word code :: rest)
  | _ -> None
;;

let uncomment_type : Cs_type.t = Mono (Fun ([ Free "Wrd" ], [ Free "Wrd" ]))
let token_re = Str.regexp "[ \t\n\r]+"

let tokenize : stack -> stack option = function
  | Word w :: rest ->
    let f s = Word (String.trim s) in
    let words = Str.split token_re w |> List.map f in
    Some (Stack words :: rest)
  | _ -> None
;;

let tokenize_type : Cs_type.t =
  Mono (Fun ([ Free "Wrd" ], [ Cs_type.Stack [ Free "Wrd" ] ]))
;;

let undocument : stack -> stack option = function
  | Word w :: rest ->
    let re = Str.regexp "\r?\n%?>> (.*)\r?\n" in
    let rec get_all_matches i : string list =
      match Str.search_forward re w i with
      | i -> Str.matched_string w :: get_all_matches (i + 1)
      | exception Not_found -> []
    in
    let f acc s = acc ^ "\n" ^ s in
    let code = get_all_matches 0 |> List.fold_left f "" in
    Some (Word code :: rest)
  | _ -> None
;;

let undocument_type : Cs_type.t = Mono (Fun ([ Free "Wrd" ], [ Free "Wrd" ]))
