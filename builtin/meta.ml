open Common
open Item

let call : stack -> stack option = function
  | Stack cs :: Stack (Stack quot :: rds) :: rest ->
    Some (Stack (quot @ cs) :: Stack rds :: rest)
  | _ -> None
;;

let callcc : stack -> stack option = function
  | Stack cs :: Stack (quot :: rds) :: rest ->
    Some (quot :: Stack [ Stack cs; Stack rds ] :: rest)
  | _ -> None
;;

let continue : stack -> stack option = function
  | _ :: Stack (Stack cs :: Stack ds :: _) :: rest -> Some (Stack cs :: Stack ds :: rest)
  (* | _ :: Stack (Word item :: Stack ds :: _) :: rest ->
     Some (Stack [ Word item ] :: Stack ds :: rest) *)
  | _ -> None
;;

let get_dict : stack -> stack option = function
  | cs :: Stack ds :: dict :: rest -> Some (cs :: Stack (dict :: ds) :: dict :: rest)
  | _ -> None
;;

let set_dict : stack -> stack option = function
  | cs :: Stack (dict :: rds) :: _ :: rest ->
    (* show_item dict |> Printf.printf "set-dict called with dict = %s\n"; *)
    Some (cs :: Stack rds :: dict :: rest)
  | _ -> None
;;
