open Common
open Item

(* TODO: Http *)

(** slurp ( source -- word ) *)
val slurp : stack -> stack option

val slurp_type : Cs_type.t

(** spit ( data-wrd file-wrd -- ) *)
val spit : stack -> stack option

val spit_type : Cs_type.t

(** spit-on ( data-wrd file-wrd -- ) *)
val spit_on : stack -> stack option

val spit_on_type : Cs_type.t
