open Common
open Item

let mapping : stack -> stack option = function
  | Stack s :: rest ->
    let rec aux (rest : stack) (acc : (item * item) list) : (item * item) list option =
      match rest with
      | [] -> Some (List.rev acc)
      | x :: y :: r -> aux r ((x, y) :: acc)
      | _ -> None
    in
    (match aux s [] with
     | Some mapping -> Some (Map (List.to_seq mapping |> Hashtbl.of_seq) :: rest)
     | None -> None)
  | _ -> None
;;

let mapping_type : Cs_type.t =
  Poly
    ( [ "a" ]
    , Fun ([ Cs_type.Stack [ Bound 0 ] ], [ Cs_type.Map ([ Bound 0 ], [ Bound 0 ]) ]) )
;;

let unmap : stack -> stack option = function
  | Map m :: rest ->
    let f acc = function
      | x, y -> x :: y :: acc
    in
    let unmapped = m |> Hashtbl.to_seq |> Seq.fold_left f [] in
    Some (Stack unmapped :: rest)
  | _ -> None
;;

let unmap_type : Cs_type.t =
  Poly
    ( [ "a"; "b" ]
    , Fun ([ Map ([ Bound 1 ], [ Bound 0 ]) ], [ Cs_type.Stack [ Bound 1; Bound 0 ] ]) )
;;

let assoc : stack -> stack option = function
  | Map m :: key :: v :: rest ->
    let m' = Hashtbl.copy m in
    Hashtbl.replace m' key v;
    (* Replace adds if the value is unbound. *)
    Some (Map m' :: rest)
  | _ -> None
;;

let assoc_type : Cs_type.t =
  Poly
    ( [ "a"; "b"; "c"; "d" ]
    , Fun
        ( [ Cs_type.Map ([ Bound 3 ], [ Bound 2 ]); Bound 1; Bound 0 ]
        , [ Cs_type.Map ([ Bound 3; Bound 1 ], [ Bound 2; Bound 0 ]) ] ) )
;;

let dissoc : stack -> stack option = function
  | Map m :: key :: rest ->
    let m' = Hashtbl.copy m in
    Hashtbl.remove m' key;
    Some (Map m' :: rest)
  | _ -> None
;;

let dissoc_type : Cs_type.t =
  Poly
    ( [ "a"; "b" ]
    , Fun
        ( [ Cs_type.Map ([ Bound 1 ], [ Bound 0 ]); Bound 1 ]
        , [ Cs_type.Map ([ Bound 1 ], [ Bound 0 ]) ] ) )
;;

let get : stack -> stack option = function
  | default :: Map m :: key :: rest ->
    (match Hashtbl.find_opt m key with
     | Some v -> Some (v :: rest)
     | None -> Some (default :: rest))
  | _ -> None
;;

let get_type : Cs_type.t =
  Poly
    ( [ "a"; "b" ]
    , Fun ([ Bound 0; Cs_type.Map ([ Bound 1 ], [ Bound 0 ]); Bound 1 ], [ Bound 0 ]) )
;;

let keys : stack -> stack option = function
  | Map m :: rest ->
    let keys = m |> Hashtbl.to_seq_keys |> List.of_seq in
    Some (Stack keys :: rest)
  | _ -> None
;;

let keys_type : Cs_type.t =
  Poly
    ( [ "a"; "b" ]
    , Fun ([ Cs_type.Map ([ Bound 1 ], [ Bound 0 ]) ], [ Cs_type.Stack [ Bound 1 ] ]) )
;;

let merge : stack -> stack option = function
  | Map top :: Map base :: rest ->
    let result = Hashtbl.copy base in
    (* Override duplicates -> top associations are prioritized. *)
    top |> Hashtbl.to_seq |> Hashtbl.replace_seq result;
    Some (Map result :: rest)
  | _ -> None
;;

let merge_type : Cs_type.t =
  Poly
    ( [ "k1"; "v1"; "k2"; "v2" ]
    , Fun
        ( [ Cs_type.Map ([ Bound 3 ], [ Bound 2 ])
          ; Cs_type.Map ([ Bound 1 ], [ Bound 0 ])
          ]
        , [ Cs_type.Map ([ Bound 3; Bound 1 ], [ Bound 2; Bound 0 ]) ] ) )
;;
