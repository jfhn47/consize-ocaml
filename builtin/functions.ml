open Common
open Item

(** The interpreter is directed by the top item of the call stack.
    * - Words get substituted with their value from the dictionary
    *   (and potentially applied if its a function) or read with the
    *   special function 'read-word'.
    * - A function object on the stack is considered a meta-function.
    *   It its applied to the whole meta context instead of just the
    *   data stack.
    * - Other items get pushed onto the data stack. Maps are read
    *   with 'read-mapping'. *)
let stepcc : stack -> stack option = function
  | Stack (item :: rcs) :: Stack ds :: Map dict :: rest_meta ->
    let aux =
      match item with
      | Word word ->
        (match Hashtbl.find_opt dict (Word word) with
         | Some (Stack s) -> Some (Stack (s @ rcs) :: Stack ds :: Map dict :: rest_meta)
         | Some (Fun (f, _)) ->
           f ds |> Option.map (fun ds -> Stack rcs :: Stack ds :: Map dict :: rest_meta)
         | _ ->
           Some
             (Stack (Word "read-word" :: rcs)
              :: Stack (item :: ds)
              :: Map dict
              :: rest_meta))
      | Fun (f, _) -> f (Stack rcs :: Stack ds :: Map dict :: rest_meta)
      | Map _ ->
        Some
          (Stack (Word "read-mapping" :: rcs)
           :: Stack (item :: ds)
           :: Map dict
           :: rest_meta)
      | _ -> Some (Stack rcs :: Stack (item :: ds) :: Map dict :: rest_meta)
    in
    (match aux with
     | Some ds -> Some ds
     | None -> Some (Stack (Word "error" :: item :: rcs) :: Stack ds :: rest_meta))
  | ds -> Some (Word "error" :: ds)
;;

let apply : stack -> stack option = function
  | Fun (f, _) :: Stack s :: rest -> f s |> Option.map (fun s -> Stack s :: rest)
  | _ -> None
;;

let apply_type : Cs_type.t =
  Poly
    ( [ "a"; "b" ]
    , Fun ([ Stack [ Bound 1 ]; Fun ([ Bound 1 ], [ Bound 0 ]) ], [ Stack [ Bound 0 ] ])
    )
;;

let compose : stack -> stack option = function
  | Fun (f, _) :: Fun (g, _) :: rest ->
    let h ds = g ds |> Fun.flip Option.bind (fun ds -> f ds) in
    Some (Fun (h, Mono Unknown) :: rest)
  (* TODO: Get the real type. *)
  | _ -> None
;;

let compose_type : Cs_type.t =
  Poly
    ( [ "a"; "b"; "c" ]
    , Fun
        ( [ Fun ([ Bound 1 ], [ Bound 0 ]); Fun ([ Bound 2 ], [ Bound 1 ]) ]
        , [ Fun ([ Bound 2 ], [ Bound 0 ]) ] ) )
;;

let func : stack -> stack option = function
  | Map dict :: Stack quot :: rest ->
    let rec runcc (cs : stack) (ds : stack) (dict : dictionary) : stack option =
      match cs with
      | [] -> Some ds
      | _ ->
        (match stepcc [ Stack cs; Stack ds; Map dict ] with
         | Some [ Stack cs; Stack ds; Map dict ] -> runcc cs ds dict
         | stack_with_error -> stack_with_error)
    in
    let f (ds : stack) : stack option = runcc quot ds dict in
    Some (Fun (f, Mono Unknown) :: rest)
  (* TODO: Get the real type. *)
  | _ -> None
;;

let func_type : Cs_type.t =
  Poly
    ( [ "a"; "k"; "v" ]
    , Fun
        ( [ Stack [ Bound 2 ]; Map ([ Bound 1 ], [ Bound 0 ]) ]
        , [ Fun ([ Bound 2 ], [ Bound 2 ]) ] ) )
;;
