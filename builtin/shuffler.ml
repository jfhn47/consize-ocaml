open Common
open Item

let swap : stack -> stack option = function
  | x :: y :: rest -> Some (y :: x :: rest)
  | _ -> None
;;

let swap_type : Cs_type.t =
  Poly ([ "a"; "b" ], Fun ([ Bound 1; Bound 0 ], [ Bound 0; Bound 1 ]))
;;

let dup : stack -> stack option = function
  | item :: rest -> Some (item :: item :: rest)
  | _ -> None
;;

let dup_type : Cs_type.t = Poly ([ "a" ], Fun ([ Bound 0 ], [ Bound 0; Bound 0 ]))

let drop : stack -> stack option = function
  | _ :: rest -> Some rest
  | _ -> None
;;

let drop_type : Cs_type.t = Poly ([ "a" ], Fun ([ Bound 0 ], []))

let rot : stack -> stack option = function
  | z :: y :: x :: rest -> Some (x :: z :: y :: rest)
  | _ -> None
;;

let rot_type : Cs_type.t =
  Poly
    ([ "a"; "b"; "c" ], Fun ([ Bound 2; Bound 1; Bound 0 ], [ Bound 0; Bound 2; Bound 1 ]))
;;
