open Common
open Item

(* TODO: Meta types *)

(** call ( [ quot & ds ] cs -- ds [ quot & cs ] ) *)
val call : stack -> stack option

(** call/cc ( [ quot & ds ] cs -- [ cd ds ] quot )
    call/cc : @RDS [ @Q ] | call/cc @RCS => [ @RDS ] [ @RCS ] | @Q  *)
val callcc : stack -> stack option

(** continue ( [ cs ds & r ] quot -- ds cs ) *)
val continue : stack -> stack option

(** get-dict ( dict ds cs -- dict [ dict & ds ] cs ) *)
val get_dict : stack -> stack option

(** set-dict ( dict [ dict' & ds ] cs -- dict' ds cs ) *)
val set_dict : stack -> stack option
