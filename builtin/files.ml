open Common
open Item

let slurp : stack -> stack option = function
  | Word source :: rest ->
    (* Use open_in_bin to be Windows compatible. *)
    let ch = open_in_bin source in
    let content = really_input_string ch (in_channel_length ch) in
    close_in ch;
    Some (Word content :: rest)
  | _ -> None
;;

let slurp_type : Cs_type.t = Mono (Fun ([ Free "Wrd" ], [ Free "Wrd" ]))

let spit : stack -> stack option = function
  | Word file :: Word data :: rest ->
    let ch = open_out file in
    output_string ch data;
    close_out ch;
    Some rest
  | _ -> None
;;

let spit_type : Cs_type.t = Mono (Fun ([ Free "Wrd"; Free "Wrd" ], []))

let spit_on : stack -> stack option = function
  | Word file :: Word data :: rest ->
    let ch = open_out_gen [ Open_wronly; Open_append; Open_creat ] 0 file in
    output_string ch data;
    close_out ch;
    Some rest
  | _ -> None
;;

let spit_on_type : Cs_type.t = Mono (Fun ([ Free "Wrd"; Free "Wrd" ], []))
