open Common
open Item

(** mapping ( stk -- map ) *)
val mapping : stack -> stack option

val mapping_type : Cs_type.t

(** unmap ( map -- stk ) *)
val unmap : stack -> stack option

val unmap_type : Cs_type.t

(** assoc ( val key map -- map' )
  * ∀ a b c d . d c { [ a ] [ b ] } -- { [ a c ] [ b d ] } *)
val assoc : stack -> stack option

val assoc_type : Cs_type.t

(** dissoc ( key map -- map' ) *)
val dissoc : stack -> stack option

val dissoc_type : Cs_type.t

(** get ( key map default -- val/default ) *)
val get : stack -> stack option

val get_type : Cs_type.t

(** keys ( map -- seq ) *)
val keys : stack -> stack option

val keys_type : Cs_type.t

(** merge ( map1 map2 -- map3 ) *)
val merge : stack -> stack option

val merge_type : Cs_type.t
