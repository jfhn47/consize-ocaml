open Common
open Item

(** The interpreter function, exposed to be used in the main function. *)
val stepcc : stack -> stack option

(** apply ( stk fct -- stk' )
    * ∀ a b . [ a ] [ a -- b ] -- [ b ] *)
val apply : stack -> stack option

val apply_type : Cs_type.t

(** compose ( fct1 fct2 -- fct3 )
    * ∀ a b c . [ b -- c ] [ a -- b ] -- [ a -- c ] *)
val compose : stack -> stack option

val compose_type : Cs_type.t

(** func ( quot dict -- fct )
  * ∀ a k v . [ a ] { [ k ] [ v ] } -- [ a -- a ] *)
val func : stack -> stack option

val func_type : Cs_type.t
